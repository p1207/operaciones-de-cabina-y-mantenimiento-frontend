import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmptyLayoutComponent } from './layouts/empty-layout/empty-layout.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { ControlConfigutarionsComponent } from './modules/controls/control-configutarions/control-configutarions.component';
import { FlightControlAssignmentComponent } from './modules/controls/flight-control-assignment/flight-control-assignment.component';
import { UpdateControlComponent } from './modules/controls/update-control/update-control.component';
import { EmergencyTypeConfigurationsComponent } from './modules/emergencies/emergency-type-configurations/emergency-type-configurations.component';
import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './modules/login/login.component';
import { ListManteinanceRequestsComponent } from './modules/manteinance/list-manteinance-requests/list-manteinance-requests.component';
import { MaintenanceConfigurationsComponent } from './modules/manteinance/maintenance-configurations/maintenance-configurations.component';
import { TechnicalMaintenanceComponent } from './modules/manteinance/technical-maintenance/technical-maintenance.component';
import { FlightStatusStatisticsByAircraaftComponent } from './modules/reports/flight-status-statistics-by-aircraaft/flight-status-statistics-by-aircraaft.component';
import { FuelStatisticsComponent } from './modules/reports/fuel-statistics/fuel-statistics.component';
import { PlannedVsLandedComponent } from './modules/reports/planned-vs-landed/planned-vs-landed.component';
import { RouteRankingComponent } from './modules/reports/route-ranking/route-ranking.component';
import { AddSupplyComponent } from './modules/supplies/add-supply/add-supply.component';
import { FlightSupplyAssignmentComponent } from './modules/supplies/flight-supply-assignment/flight-supply-assignment.component';
import { SuppliesComponent } from './modules/supplies/update-supply/supplies.component';
import {ControlsReportComponent} from './modules/reports/controls-report/controls-report.component';
import {SaleSuppliesReportComponent} from './modules/reports/sale-supplies-report/sale-supplies-report.component';
import {CateringSuppliesReportComponent} from './modules/reports/catering-supplies-report/catering-supplies-report.component'
import {TechnicianReportComponent} from './modules/reports/technician-report/technician-report.component'
import { NotificationsComponent } from './modules/notifications/notifications.component';
import { EmergencyComponent } from './modules/emergencies/emergency/emergency.component';
import { AuditsComponent } from './modules/audits/audits.component';
import { MaintenanceAssignmentComponent } from './modules/manteinance/maintenance-assignment/maintenance-assignment.component';
import { UserInfoComponent } from './modules/user-info/user-info.component';
import { ProtocolConfigurationsComponent } from './modules/protocols/protocol-configurations/protocol-configurations.component';
const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path:'', component: EmptyLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  },
  {
    path: '', component: MainLayoutComponent,
    children: [
      {
        path: 'list-manteinance-requests',
        component : ListManteinanceRequestsComponent
      },
      {
        path: 'maintenance-configurations',
        component : MaintenanceConfigurationsComponent
      },
      {
        path: 'home',
        component : HomeComponent
      },
      {
        path: 'technical-maintenance',
        component : TechnicalMaintenanceComponent
      },
      {
        path: 'update-supply',
        component : SuppliesComponent
      },
      {
        path: 'add-supply',
        component : AddSupplyComponent
      },
      {
        path: 'flight-supply-assignment',
        component : FlightSupplyAssignmentComponent
      },
      {
        path: 'control-configurations',
        component : ControlConfigutarionsComponent
      },
      {
        path: 'flight-control-assignment',
        component : FlightControlAssignmentComponent
      },
      {
        path: 'update-control',
        component : UpdateControlComponent
      },
      {
        path: 'route-ranking',
        component : RouteRankingComponent
      },
      {
        path: 'flight-status-statistics-by-aircraaft',
        component : FlightStatusStatisticsByAircraaftComponent
      },
      {
        path: 'planned-vs-landed',
        component : PlannedVsLandedComponent
      },
      {
        path: 'fuel-statistics',
        component : FuelStatisticsComponent
      },
      {
        path: 'controls-report',
        component : ControlsReportComponent
      },
      {
        path: 'sale-supplies-report',
        component : SaleSuppliesReportComponent
      },
      {
        path: 'catering-supplies-report',
        component : CateringSuppliesReportComponent
      },
      {
        path: 'technician-report',
        component : TechnicianReportComponent
      },
      {
        path: 'emergency-configurations',
        component : EmergencyComponent
      },
      {
        path: 'emergency-type-configuration',
        component : EmergencyTypeConfigurationsComponent
      },
      {
        path: 'notifications',
        component : NotificationsComponent
      },
      {
        path: 'audits',
        component : AuditsComponent
      },
      {
        path: 'maintenance-assignment',
        component : MaintenanceAssignmentComponent
      },
      {
        path: 'protocol-configurations',
        component : ProtocolConfigurationsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
