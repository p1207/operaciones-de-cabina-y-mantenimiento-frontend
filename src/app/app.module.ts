import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { HeaderComponent } from './layouts/header/header.component';
import { SideNavBarComponent } from './layouts/side-nav-bar/side-nav-bar.component';
import { AngularMaterialModule } from './shared/angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppDatePipe } from './core/pipes/app-date-pipe';
import { HomeComponent } from './modules/home/home.component';

import { ListManteinanceRequestsComponent } from './modules/manteinance/list-manteinance-requests/list-manteinance-requests.component';
import { AddManteinanceRequestComponent } from './modules/manteinance/add-manteinance-request/add-manteinance-request.component';
import { MaintenanceConfigurationsComponent } from './modules/manteinance/maintenance-configurations/maintenance-configurations.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AddMaintenanceComponent } from './modules/manteinance/add-maintenance/add-maintenance.component';
import { TechnicalMaintenanceComponent } from './modules/manteinance/technical-maintenance/technical-maintenance.component';
import { SendTechnicalMaintenanceComponent } from './modules/manteinance/send-technical-maintenance/send-technical-maintenance.component';
import { AlertDialogComponent } from './modules/manteinance/alert-dialog/alert-dialog.component';
import { LoginComponent } from './modules/login/login.component';
import { EmptyLayoutComponent } from './layouts/empty-layout/empty-layout.component';

import { ControlConfigutarionsComponent } from './modules/controls/control-configutarions/control-configutarions.component';
import { AddControlComponent } from './modules/controls/add-control/add-control.component';
import { AddSupplyComponent } from './modules/supplies/add-supply/add-supply.component';
import { CateringComponent } from './modules/supplies/update-supply/catering/catering.component';
import { SanitaryComponent } from './modules/supplies/update-supply/sanitary/sanitary.component';
import { SaleComponent } from './modules/supplies/update-supply/sale/sale.component';
import { SuppliesComponent } from './modules/supplies/update-supply/supplies.component';
import { FlightSupplyAssignmentComponent } from './modules/supplies/flight-supply-assignment/flight-supply-assignment.component';
import { FlightControlAssignmentComponent } from './modules/controls/flight-control-assignment/flight-control-assignment.component';
import { ControlViewComponent } from './modules/controls/control-view/control-view.component';
import { UpdateControlComponent } from './modules/controls/update-control/update-control.component';
import { RouteRankingComponent } from './modules/reports/route-ranking/route-ranking.component';
import { FlightStatusStatisticsByAircraaftComponent } from './modules/reports/flight-status-statistics-by-aircraaft/flight-status-statistics-by-aircraaft.component';
import { PassengersListComponent } from './modules/supplies/passengers-list/passengers-list.component';
import { FuelStatisticsComponent } from './modules/reports/fuel-statistics/fuel-statistics.component';
import { PlannedVsLandedComponent } from './modules/reports/planned-vs-landed/planned-vs-landed.component';
import { CateringSuppliesReportComponent } from './modules/reports/catering-supplies-report/catering-supplies-report.component';
import { EmergencyTypeConfigurationsComponent } from './modules/emergencies/emergency-type-configurations/emergency-type-configurations.component';
import { ControlsReportComponent } from './modules/reports/controls-report/controls-report.component';
import { SaleSuppliesReportComponent } from './modules/reports/sale-supplies-report/sale-supplies-report.component';
import { TechnicianReportComponent } from './modules/reports/technician-report/technician-report.component';
import { NotificationsComponent } from './modules/notifications/notifications.component';
import { PaginatePipe } from './core/pipes/paginate.pipe';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from './shared/paginator';
import { ErrorDialogComponent } from './core/dialogs/error-dialog/error-dialog.component';
import { ConfirmLogoutDialogComponent } from './core/dialogs/confirm-logout-dialog/confirm-logout-dialog.component';
import { EmptyStateComponent } from './modules/empty-state/empty-state.component';
import { EmergencyComponent } from './modules/emergencies/emergency/emergency.component';
import { AuditsComponent } from './modules/audits/audits.component';
import { AddEmergencyTypeComponent } from './modules/emergencies/add-emergency-type/add-emergency-type.component';
import { AddEmergencyComponent } from './modules/emergencies/add-emergency/add-emergency.component';
import { MaintenanceAssignmentComponent } from './modules/manteinance/maintenance-assignment/maintenance-assignment.component';
import { UserInfoComponent } from './modules/user-info/user-info.component';
import { ProtocolConfigurationsComponent } from './modules/protocols/protocol-configurations/protocol-configurations.component';
import { Interceptor } from './core/interceptors/interceptor';


@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    HeaderComponent,
    SideNavBarComponent,
    ListManteinanceRequestsComponent,
    AppDatePipe,
    AddManteinanceRequestComponent,
    MaintenanceConfigurationsComponent,
    HomeComponent,
    AddMaintenanceComponent,
    TechnicalMaintenanceComponent,
    SendTechnicalMaintenanceComponent,
    AlertDialogComponent,
    LoginComponent,
    EmptyLayoutComponent,
    SuppliesComponent,
    SaleComponent,
    CateringComponent,
    SanitaryComponent,
    ControlConfigutarionsComponent,
    AddControlComponent,
    AddSupplyComponent,
    FlightSupplyAssignmentComponent,
    FlightControlAssignmentComponent,
    ControlViewComponent,
    UpdateControlComponent,
    RouteRankingComponent,
    FlightStatusStatisticsByAircraaftComponent,
    PassengersListComponent,
    FuelStatisticsComponent,
    PlannedVsLandedComponent,
    ControlsReportComponent,
    SaleSuppliesReportComponent,
    CateringSuppliesReportComponent,
    TechnicianReportComponent,
    PlannedVsLandedComponent,
    EmergencyTypeConfigurationsComponent,
    NotificationsComponent,
    PaginatePipe,
    ErrorDialogComponent,
    ConfirmLogoutDialogComponent,
    EmptyStateComponent,
    EmergencyComponent,
    AuditsComponent,
    AddEmergencyTypeComponent,
    AddEmergencyComponent,
    MaintenanceAssignmentComponent,
    UserInfoComponent,
    ProtocolConfigurationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports:[
    AppDatePipe,
    PaginatePipe
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
