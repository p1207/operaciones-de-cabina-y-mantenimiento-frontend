import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddManteinanceRequestComponent } from './add-manteinance-request.component';

describe('AddManteinanceRequestComponent', () => {
  let component: AddManteinanceRequestComponent;
  let fixture: ComponentFixture<AddManteinanceRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddManteinanceRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddManteinanceRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
