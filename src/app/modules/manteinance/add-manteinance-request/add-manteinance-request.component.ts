import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { AbilityDTO, AircraftDTO, MaintenanceDTO, MaintenanceRequestDTO, MaintenanceTaskDTO, TechnicianDTO } from 'src/app/core/model/common.class';
import { MaintenanceRequestStatus } from 'src/app/core/model/enum.class';
import { AircraftService } from 'src/app/core/services/aircraft.service';
import { MaintenanceRequestService } from 'src/app/core/services/maintenance-request.service';
import { MaintenancesService } from 'src/app/core/services/maintenances.service';
import { TechnicianService } from 'src/app/core/services/technic.service';

@Component({
  selector: 'app-add-manteinance-request',
  templateUrl: './add-manteinance-request.component.html',
  styleUrls: ['./add-manteinance-request.component.css']
})
export class AddManteinanceRequestComponent implements OnInit {

  public availablePriorities = ["ALTA","MEDIA","BAJA"];
  public availableTechnicians?: TechnicianDTO[];
  public availableAircrafts?: AircraftDTO[];
  public loading!: boolean
  public controls!: FormGroup;
  public displayedColumns = ["description","remove"];
  public dataSource!: MatTableDataSource<MaintenanceTaskDTO>;
  public taskControl!: FormControl;
  public maintenanceByAircraft!: MaintenanceDTO[];
  public loadingMaintenances!: boolean;

  constructor(
    private aircraftService: AircraftService,
    private technicianService: TechnicianService,
    private maintenanceService: MaintenancesService,) {
      let controls = {
        aircraft: new FormControl('', Validators.required),
        maintenance: new FormControl('', Validators.required),
        description: new FormControl(null, Validators.required),
        priority: new FormControl(null, Validators.required),
        module: new FormControl('', Validators.required),
        technisian: new FormControl('', Validators.required),
      }
      this.controls = new FormGroup(controls);
      this.taskControl = new FormControl(null, Validators.required);
    }

  ngOnInit(): void {
    this._setAsyncData();
    this._setDataSource();
  }

  private _setDataSource(): void {
    let emptyList: MaintenanceTaskDTO[] = []
    this.dataSource = new MatTableDataSource(emptyList);
  }

  private async _setAsyncData(): Promise<void>{
    this._setLoading(true);
    this.availableAircrafts = await this.aircraftService.getAircrafts().toPromise();
    this.availableTechnicians = await this.technicianService.getTechnicians().toPromise();
    this._setLoading(false);
  }

  private _setLoading(value: boolean): void{
    this.loading = value;
  }

  public getAbilitiesString(abilities: AbilityDTO[]): string {
    return abilities.map(ability => ability.Name).join(", ");
  }

  public setMaintenance(): void {
    var id= this.controls.get("aircraft")?.value?.Id;
    this.loadingMaintenances = true;
    this.maintenanceService.getMaintenancesByAircraft(id).subscribe(data =>{
      this.loadingMaintenances = false;
      this.maintenanceByAircraft = data?.map(data => data.Maintenance);
    });
  }

  public disableMaintenanceSelect(): boolean {
    return Boolean(this.controls.get("aircraft")?.value) == false || this.loadingMaintenances;
  }

  public deleteTask(task: MaintenanceTaskDTO): void {
    const index = this.dataSource.data.indexOf(task);
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }

  public getMaintenanceReqDTO(): MaintenanceRequestDTO {
    let request: MaintenanceRequestDTO = new MaintenanceRequestDTO();
    request.Aircraft = this.controls.get("aircraft")?.value.Id
    request.AircraftId = this.controls.get("aircraft")?.value.Id;
    request.MaintenanceId = this.controls.get("maintenance")?.value.Id;
    request.TechnicianId = this.controls.get("technisian")?.value.Id;
    request.Description = this.controls.get("description")?.value;
    request.Priority = this.controls.get("priority")?.value;
    request.Module = this.controls.get("module")?.value;
    request.Tasks = this.dataSource.data;
    request.Status = MaintenanceRequestStatus.REGISTERED;
    request.Id ??= null;
    return request;
  }

  public addTask(): void {
    let task: MaintenanceTaskDTO = { Id: null, Description: this.taskControl.value };
    this.dataSource.data.push(task);
    this.dataSource._updateChangeSubscription();
    this.taskControl?.reset();
  }

  public duplicateTask(): boolean {
    return this.dataSource.data.some(task => task.Description == this.taskControl.value);
  }
}
