import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { AircraftDTO, MaintenanceByAircraftDTO, MaintenanceDTO } from 'src/app/core/model/common.class';
import { AircraftService } from 'src/app/core/services/aircraft.service';
import { MaintenancesService } from 'src/app/core/services/maintenances.service';

@Component({
  selector: 'app-maintenance-assignment',
  templateUrl: './maintenance-assignment.component.html',
  styleUrls: ['./maintenance-assignment.component.css']
})
export class MaintenanceAssignmentComponent implements OnInit {

  public controls!: FormGroup;
  public availableAircrafts!: AircraftDTO[];
  public availableMaintenances!: MaintenanceDTO[];
  public loadingTable!: boolean;
  public dataSource!: MatTableDataSource<MaintenanceDTO>;
  public displayedColumns = ["name", "type"];

  constructor(
    private aircraftService: AircraftService,
    private maintenanceService: MaintenancesService,
    ) { }

  ngOnInit(): void {
    this._createControls();
    this._getAircrafts();
    this._getMaintenances();
  }

  private _getAircrafts() {
    this.aircraftService.getAircrafts().subscribe(data => this.availableAircrafts = data);
  }

  public changeAircraftLogic(): void {
    this.loadingTable = true;
    this._setMaintenancesByAircraftDataSource();
    this._setEnableControls();
  }

  public invalidForms(): boolean {
    return this.controls.invalid;
  }

  public cleanControl(): void {
    this.controls.get("maintenance")?.reset();
  }

  public doSave(): void {
    this.maintenanceService.saveMaintenanceByAircraft(this._getMaintennaceByAircraftDto()).subscribe(() => this._refreshView());
  }

  public filterMaintenances(maintenances: MaintenanceDTO[]): MaintenanceDTO[] {
    let dataSourceMaintenancesIds = this.dataSource?.data.map(data => data.Id);
    return maintenances?.filter(data => !dataSourceMaintenancesIds?.includes(data.Id));
  }

  private _setMaintenancesByAircraftDataSource() {
    let aircraftId = this.controls.get("aircraft")?.value?.Id
    this.maintenanceService.getMaintenancesByAircraft(aircraftId).subscribe(data => this._setDataSource(data));
  }

  private _setDataSource(maintenances: MaintenanceByAircraftDTO[]): void {
    this.loadingTable = false;
    this.dataSource = new MatTableDataSource(maintenances.map(maintenanceByAircraft => maintenanceByAircraft.Maintenance));
  }

  private _getMaintenances() {
    this.maintenanceService.getMaintenances().subscribe(data => this.availableMaintenances = data);
  }

  private _createControls(): void {
    let controls = {
      aircraft: new FormControl(null, Validators.required),
      maintenance: new FormControl(null, Validators.required)
    };
    this.controls = new FormGroup(controls);
  }

  private _refreshView(): void {
    this.loadingTable = true;
    this._setMaintenancesByAircraftDataSource();
  }

  private _setEnableControls(): void {
    this.controls.get("maintenance")?.enable();
  }

  private _getMaintennaceByAircraftDto(): MaintenanceByAircraftDTO {
    let maintenance: MaintenanceByAircraftDTO = new MaintenanceByAircraftDTO();
    maintenance.Id = null;
    maintenance.Aircraft = this.controls.get('aircraft')?.value.Id;
    maintenance.Maintenance = this.controls.get('maintenance')?.value;
    return maintenance;
  }

}
