import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceAssignmentComponent } from './maintenance-assignment.component';

describe('MaintenanceAssignmentComponent', () => {
  let component: MaintenanceAssignmentComponent;
  let fixture: ComponentFixture<MaintenanceAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaintenanceAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
