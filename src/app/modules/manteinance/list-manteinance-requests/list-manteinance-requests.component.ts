import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { AddManteinanceRequestComponent } from '../add-manteinance-request/add-manteinance-request.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';
import { MaintenanceRequestService } from 'src/app/core/services/maintenance-request.service';
import { MaintenanceRequestDTO } from 'src/app/core/model/common.class';

@Component({
  selector: 'app-list-manteinance-requests',
  templateUrl: './list-manteinance-requests.component.html',
  styleUrls: ['./list-manteinance-requests.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class ListManteinanceRequestsComponent implements OnInit {

  public displayedColumns: string[] = ['id', 'date', 'priority', "status", "duration", "aircraftName", "technician", "observation", "description"];
  public dataSource!: MatTableDataSource<MaintenanceRequestDTO>;
  public maintenanceReqData!: MaintenanceRequestDTO[];
  public loading!: boolean;

  public readonly priorityTexts: { [key: number]: string } = {
    0: "ALTO",
    1: "MEDIO",
    2: "BAJO"
  };

  public readonly statusTexts: { [key: number]: string } = {
    0: "REGISTRADA",
    1: "EN_PROGRESO",
    2: "PENDIENTE",
    3: "APROBADA"
  };

  public readonly availableStatus = ["REGISTRADA","EN_PROGRESO",  "PENDIENTE", "APROBADA"];

  constructor(
    private maintenanceService: MaintenanceRequestService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar) { }

  public ngOnInit(): void {
    this._setData();
  }

  private _setData(): void {
    this.loading = true;
    this.maintenanceService.getMaintenanceRequests().subscribe(data => {
      this.loading = false;
      this.maintenanceReqData = data;
      this.dataSource = new MatTableDataSource(this.maintenanceReqData);
    });
  }

  public sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id':
          return compare(a.Id, b.Id, isAsc);
        case 'date':
          return compare(new Date(a.Date).getTime(), new Date(b.Date).getTime(), isAsc);
        case 'priority':
          return compare(a.Priority, b.Priority, isAsc);
        case 'status':
          return compare(a.Status, b.Status, isAsc);
        default:
          return 0;
      }
    });
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(AddManteinanceRequestComponent, {
      width: '900px',
      height: '600px'
    });

    dialogRef.afterClosed().subscribe(maintenanceRequest => {
      if(maintenanceRequest != "false" &&  maintenanceRequest != undefined){
        this.maintenanceService.saveMaintenanceRequest(maintenanceRequest).subscribe(() => this._refreshView());
      }
    });
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public saveMaintenanceStatus(maintenance: MaintenanceRequestDTO): void {
    if(maintenance) {
      this.maintenanceService.saveMaintenanceRequest(maintenance).subscribe(() => this._openSnackBar());
    }
  }

  private _openSnackBar(): void {
    this._snackBar.open("Estado guardado", "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

  private _refreshView(): void {
    this.ngOnInit();
  }
}

function compare(a: number | string | null, b: number | string | null, isAsc: boolean) {
  return (a! < b! ? -1 : 1) * (isAsc ? 1 : -1);
}

