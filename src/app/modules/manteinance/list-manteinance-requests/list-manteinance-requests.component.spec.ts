import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListManteinanceRequestsComponent } from './list-manteinance-requests.component';


describe('MaintenanceComponent', () => {
  let component: ListManteinanceRequestsComponent;
  let fixture: ComponentFixture<ListManteinanceRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListManteinanceRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListManteinanceRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
