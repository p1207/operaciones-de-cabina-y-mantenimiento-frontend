import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AbilityDTO, MaintenanceDTO, MaintenanceTaskDTO } from 'src/app/core/model/common.class';
import { CommonService } from 'src/app/core/services/common.service';
import { MaintenancesService } from 'src/app/core/services/maintenances.service';
import { AddMaintenanceComponent } from '../add-maintenance/add-maintenance.component';

@Component({
  selector: 'app-maintenance-configurations',
  templateUrl: './maintenance-configurations.component.html',
  styleUrls: ['./maintenance-configurations.component.css']
})
export class MaintenanceConfigurationsComponent implements OnInit {

  public dataSource!: MatTableDataSource<MaintenanceDTO>;
  public maintenancesData!: MaintenanceDTO[];
  public tasksData!: MaintenanceTaskDTO[];
  public loading!: boolean;

  public pageSize = 5;
  public pageNumber = 1;
  public pageSizeOptions = [5,10,20,50];

  constructor(
    private maintenanceService: MaintenancesService,
    private commonService: CommonService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this._setData();
  }

  private _setData(): void {
    this._setLoading(true);
    this.maintenanceService.getMaintenances().subscribe(data =>{
      this.maintenancesData = data;
      this.dataSource = new MatTableDataSource(this.maintenancesData);
      this._setLoading(false);
    });
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.maintenancesData = this.dataSource.filteredData;
  }

  public handlePage(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
  }

  public delete(maintenance: MaintenanceDTO): void{
    const index = this.maintenancesData.indexOf(maintenance, 0);
    this.maintenancesData.splice(index, 1);
    this.maintenanceService.deleteMaintenance(maintenance.Id).subscribe(data =>
      this._refreshView());
  }

  public openDialog(title: string, maintenance?: MaintenanceDTO): void {
    const dialogRef = this.dialog.open(AddMaintenanceComponent, {
      width: '700px',
      height: '900px',
      data!: {maintenance: maintenance, title: title},
    });

    dialogRef.afterClosed().subscribe(maintenance => {
      if(maintenance){
        this.maintenanceService.saveMaintenance(maintenance).subscribe( data =>{
          this._refreshView();
        });
      }
    });
  }

  private _refreshView(): void {
    this.ngOnInit();
  }

  private _setLoading(value: boolean): void {
    this.loading = value;
  }

  public getAbilitiesString(abilities?: AbilityDTO[]): string {
    if(abilities) return abilities!.map(ability => ability.Name).join(", ")
    return "";
  }
}

