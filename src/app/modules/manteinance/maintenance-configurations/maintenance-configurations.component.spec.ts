import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceConfigurationsComponent } from './maintenance-configurations.component';

describe('MaintenanceConfigurationsComponent', () => {
  let component: MaintenanceConfigurationsComponent;
  let fixture: ComponentFixture<MaintenanceConfigurationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaintenanceConfigurationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
