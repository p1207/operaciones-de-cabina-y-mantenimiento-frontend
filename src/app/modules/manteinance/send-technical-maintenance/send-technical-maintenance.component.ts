import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MaintenanceRequestDTO, MaintenanceTaskDTO } from 'src/app/core/model/common.class';
import { MaintenanceRequestStatus } from 'src/app/core/model/enum.class';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-send-technical-maintenance',
  templateUrl: './send-technical-maintenance.component.html',
  styleUrls: ['./send-technical-maintenance.component.css']
})
export class SendTechnicalMaintenanceComponent implements OnInit {

  public observationsControl!: FormControl;

  public readonly priorityTexts: { [key: number]: string } = {
    0: "ALTO",
    1: "MEDIO",
    2: "BAJO"
  }

  constructor( @Inject(MAT_DIALOG_DATA) public data: MaintenanceRequestDTO,
                public dialogRef: MatDialogRef<SendTechnicalMaintenanceComponent>,
                private dialog: MatDialog) { }

  ngOnInit(): void {
    this.observationsControl = new FormControl(null,Validators.required);
    this.observationsControl.setValue(this.data.Observation);
  }

  public openAlertDialog(): void {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      height: '300px'
    });

    dialogRef.afterClosed().subscribe(doCloseAndSave => {
      if(doCloseAndSave) {
        this.data.Observation = this.observationsControl.value;
        this.data.Status = MaintenanceRequestStatus.PENDING;
        this.dialogRef.close(this.data);
      }
    });
  }

  public closeDialog(): void {
    this.dialogRef.close();
  }

  public getMaintenanceTasks(): MaintenanceTaskDTO[] {
    return this.data.Tasks.concat(this.data.Maintenance.Tasks);
  }

  public isPending(): boolean {
    return this.data.StatusId == MaintenanceRequestStatus.PENDING;
  }

}
