import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendTechnicalMaintenanceComponent } from './send-technical-maintenance.component';

describe('SendTechnicalMaintenanceComponent', () => {
  let component: SendTechnicalMaintenanceComponent;
  let fixture: ComponentFixture<SendTechnicalMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendTechnicalMaintenanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendTechnicalMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
