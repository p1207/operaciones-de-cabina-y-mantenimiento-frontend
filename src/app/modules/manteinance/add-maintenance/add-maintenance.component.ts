import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { AbilityDTO, MaintenanceDTO, MaintenanceSupplyDTO, MaintenanceTaskDTO } from 'src/app/core/model/common.class';
import { AbilityService } from 'src/app/core/services/ability.service';
import { MaintenanceSupplyService } from 'src/app/core/services/maintenance-supply.service';

@Component({
  selector: 'app-add-maintenance',
  templateUrl: './add-maintenance.component.html',
  styleUrls: ['./add-maintenance.component.css']
})
export class AddMaintenanceComponent implements OnInit {

  public taskDataSource!: MatTableDataSource<MaintenanceTaskDTO>;
  public suppliesDataSource!: MatTableDataSource<MaintenanceSupplyDTO>;
  public availableAbilities!: AbilityDTO[];
  public availableSupplies!: MaintenanceSupplyDTO[];
  public displayedColumns = ["description", "remove"];
  public controls!: FormGroup;
  public taskControl!: FormControl;
  public supplyControl!: FormControl;
  public tabIndex: number = 0;
  public title!: string;
  public readonly maintenancesTypeTexts: { [key: string]: number } = {
    "CORRECTIVO": 0,
    "PREVENTIVO": 1
  };

  constructor(
    private abilityService: AbilityService,
    private supplyService: MaintenanceSupplyService,
    private dialogRef: MatDialogRef<AddMaintenanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { maintenance: MaintenanceDTO, title: string }) {
  }

  ngOnInit(): void {
    this._setSupplies();
    this._setAbilities();
    this._createControls();
    this._setDataSource();
    this._setTitle();
  }

  private _setSupplies(): void {
    this.supplyService.getSupplies().subscribe(data => this.availableSupplies = data);
  }

  private _setAbilities(): void {
    this.abilityService.getAbilities().subscribe(data => this.availableAbilities = data);
  }

  private _setTitle(): void {
    this.title = this.data.title;
  }

  private _setDataSource(): void {
    let taskList: MaintenanceTaskDTO[] = this.data.maintenance != undefined ? this.data.maintenance.Tasks : [];
    let suppliesList: MaintenanceSupplyDTO[] = this.data.maintenance != undefined ? this.data.maintenance.Supplies : [];
    this.taskDataSource = new MatTableDataSource(taskList);
    this.suppliesDataSource = new MatTableDataSource(suppliesList);
  }

  private _createControls(): void {
    let controls = {
      ability: new FormControl(this.data?.maintenance?.Abilities.map(ability => ability.Id), Validators.required),
      description: new FormControl(this.data?.maintenance?.Description, Validators.required),
      name: new FormControl(this.data?.maintenance?.Name, Validators.required),
      type: new FormControl(this.maintenancesTypeTexts[this.data?.maintenance?.Type], Validators.required)
    }
    this.controls = new FormGroup(controls);
    this.taskControl = new FormControl(null, Validators.required);
    this.supplyControl = new FormControl(null, Validators.required);
  }

  public addTask(): void {
    let task: MaintenanceTaskDTO = { Id: null, Description: this.taskControl.value };
    this.taskDataSource.data.push(task);
    this.taskDataSource._updateChangeSubscription();
    this.taskControl?.reset();
  }

  public addSupply(): void {
    let supply: MaintenanceSupplyDTO = this.supplyControl.value;
    this.suppliesDataSource.data.push(supply);
    this.suppliesDataSource._updateChangeSubscription();
    this.supplyControl?.reset();
  }

  public add(): void {
    if (this.tabIndex == 0) {
      this.addTask();
    } else {
      this.addSupply();
    }
  }

  public disableSaveButton(): boolean | undefined {
    return this.invalidFoms() || this.emptyTaskList() || this.emptySuppliesList();
  }

  public disableButton(): boolean {
    if (this.tabIndex == 0)
      return this.taskControl.invalid || this.duplicateTask();
    return this.supplyControl.invalid || this.duplicateSupply();
  }

  public deleteTask(task: MaintenanceTaskDTO): void {
    const index = this.taskDataSource.data.indexOf(task);
    this.taskDataSource.data.splice(index, 1);
    this.taskDataSource._updateChangeSubscription();
  }

  public deleteSupply(supply: MaintenanceSupplyDTO): void {
    const index = this.suppliesDataSource.data.indexOf(supply);
    this.suppliesDataSource.data.splice(index, 1);
    this.suppliesDataSource._updateChangeSubscription();
  }

  public getTooltipMessage(): string {
    const emptyTask = "Debe agregar al menos una tarea para poder guardar";
    const emptySupplies = "Debe agregar al menos un insumo";
    const invalidControlsMessage = "Debe rellenar los campos obligatorios";
    let res: string = "";
    if (this.emptyTaskList())
      res = emptyTask;
    if (this.emptySuppliesList())
      res = emptySupplies;
    if (this.invalidFoms())
      res = invalidControlsMessage;
    return res;
  }

  public emptyTaskList(): boolean {
    return this.taskDataSource.data.length == 0;
  }

  public emptySuppliesList(): boolean {
    return this.suppliesDataSource.data.length == 0;
  }

  public invalidFoms(): boolean {
    return this.controls.invalid;
  }

  public getMaintenance(): MaintenanceDTO {
    let maintenance: MaintenanceDTO = new MaintenanceDTO();
    maintenance.Id = this.data.maintenance == null ? null : this.data.maintenance.Id;
    maintenance.Abilities = this.controls.get("ability")?.value.map((id: number) => this.availableAbilities.find(ability => ability.Id == id));
    maintenance.Description = this.controls.get("description")?.value;
    maintenance.Name = this.controls.get("name")?.value;
    maintenance.Type = this.controls.get("type")?.value;
    maintenance.Tasks = this.taskDataSource.data;
    maintenance.Supplies = this.suppliesDataSource.data;
    return maintenance;
  }

  public closeAndSave(): void {
    this.dialogRef.close(this.getMaintenance());
  }

  public duplicateTask(): boolean {
    return this.taskDataSource.data.some(task => task.Description == this.taskControl.value);
  }

  public duplicateSupply(): boolean {
    return this.suppliesDataSource.data.some(supply => supply.Id == this.supplyControl.value);
  }
}
