import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MaintenanceRequestDTO } from 'src/app/core/model/common.class';
import { MaintenanceRequestStatus } from 'src/app/core/model/enum.class';
import { MaintenanceRequestService } from 'src/app/core/services/maintenance-request.service';
import { SendTechnicalMaintenanceComponent } from '../send-technical-maintenance/send-technical-maintenance.component';

@Component({
  selector: 'app-technical-maintenance',
  templateUrl: './technical-maintenance.component.html',
  styleUrls: ['./technical-maintenance.component.css']
})
export class TechnicalMaintenanceComponent implements OnInit {

  public displayedColumns: string[] = ['id', 'date', 'priority', "status", "duration", "aircraftName", "technician", "observation", "description"];
  public dataSource!: MatTableDataSource<MaintenanceRequestDTO>;
  public maintenanceReqData!: MaintenanceRequestDTO[];
  public readonly availableStatus = [{name:"REGISTRADO", index:0}, {name:"EN_PROGRESO", index:1}];

  public readonly priorityTexts: { [key: number]: string } = {
    0: "ALTO",
    1: "MEDIO",
    2: "BAJO"
  }

  public readonly statusTexts: { [key: number]: string } = {
    0: "REGISTRADA",
    1: "EN_PROGRESO",
    2: "PENDIENTE",
    3: "APROBADA"
  };

  constructor(private _snackBar: MatSnackBar,
              private dialog: MatDialog,
              private maintenanceService: MaintenanceRequestService) { }

  ngOnInit(): void {
    this.maintenanceService.getTechnicMaintenanceRequests(1).subscribe(data => {
      this.maintenanceReqData = data;
      this.dataSource = new MatTableDataSource(this.maintenanceReqData);
    });
  }

  public sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id':
          return compare(
          a.Id, b.Id, isAsc);
        case 'date':
          return compare(a.Date.getTime(), b.Date.getTime(), isAsc);
        case 'priority':
          return compare(a.Priority, b.Priority, isAsc);
        case 'status':
          return compare(a.Status, b.Status, isAsc);
        default:
          return 0;
      }
    });
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public saveMaintenanceStatus(maintenanceRequest: MaintenanceRequestDTO): void {
    this.maintenanceService.saveMaintenanceRequest(maintenanceRequest).subscribe();
    this._openSnackBar();
  }

  public openDialog(request: MaintenanceRequestDTO): void {
    const dialogRef = this.dialog.open(SendTechnicalMaintenanceComponent, {
      width: '700px',
      height: '700px',
      data: request
    });

    dialogRef.afterClosed().subscribe(maintenanceRequest => {
      if(maintenanceRequest) {
        this.maintenanceService.saveMaintenanceRequest(maintenanceRequest).subscribe();
        this._refreshView();
      }
    });
  }

  private _openSnackBar(): void {
    this._snackBar.open("Estado guardado", "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

  private _refreshView(): void {
    this.ngOnInit();
  }

  public editableStatus(maintenanceRequest: MaintenanceRequestDTO): boolean {
    return maintenanceRequest.StatusId == MaintenanceRequestStatus.INPROGRESS ||
    maintenanceRequest.StatusId == MaintenanceRequestStatus.REGISTERED;
  }

}

function compare(a: number | string | null, b: number | string | null, isAsc: boolean) {
  return (a! < b! ? -1 : 1) * (isAsc ? 1 : -1);
}
