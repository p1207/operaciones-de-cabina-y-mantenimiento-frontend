import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalMaintenanceComponent } from './technical-maintenance.component';

describe('TechnicalMaintenanceComponent', () => {
  let component: TechnicalMaintenanceComponent;
  let fixture: ComponentFixture<TechnicalMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnicalMaintenanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
