import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SaleDTO, SanitaryDTO, SupplyDTO, CateringDTO } from 'src/app/core/model/common.class';
import { SupplyService } from 'src/app/core/services/supply.service';

@Component({
  selector: 'app-add-supply',
  templateUrl: './add-supply.component.html',
  styleUrls: ['./add-supply.component.css']
})
export class AddSupplyComponent implements OnInit {

  public availableCateringMenuType = ["Vegano", "Vegetariano", "Celiaco", "Estandar"];
  public availableSanitaryType = ["Confort", "Limpieza", "Descartable"];
  public availableSaleType = ["Bebida", "Perfumería", "Electrónica", "Joyería", "Comestible"];

  public sanitaryControls!: FormGroup;
  public cateringControls!: FormGroup;
  public saleControls!: FormGroup;

  constructor(
    private supplyService: SupplyService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this._createControls();
  }

  private _createControls(): void {
    let sanitaryControls = this._getForms(false);
    let cateringControls = this._getForms(false);
    let saleControls = this._getForms(true);
    this.sanitaryControls = new FormGroup(sanitaryControls);
    this.cateringControls = new FormGroup(cateringControls);
    this.saleControls = new FormGroup(saleControls);
  }

  private _getForms(hasPrice: boolean): { [key: string]: FormControl } {
    return {
      name: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required),
      price: hasPrice ? new FormControl(null, [Validators.required, Validators.min(1)]) : new FormControl(null)
    }
  }

  public cleanControl(formGroup: FormGroup): void {
    formGroup.reset();
    formGroup.clearAsyncValidators();
    this._createControls();
  }

  public saveCateringSupply(): void {
    let supply: CateringDTO = new CateringDTO();
    supply.Description = this.cateringControls.get("description")?.value;
    supply.Name = this.cateringControls.get("name")?.value;
    supply.MenuType = this.cateringControls.get("type")?.value;
    supply.Id = null;
    this.supplyService.saveSupply(supply,"/Catering/save").subscribe(() => this._openSnackBar());
    this.cleanControl(this.cateringControls);
  }

  public saveSanitarySupply(): void {
    let supply: SanitaryDTO = new SanitaryDTO();
    supply.Description = this.sanitaryControls.get("description")?.value;
    supply.Name = this.sanitaryControls.get("name")?.value;
    supply.SanitaryType = this.sanitaryControls.get("type")?.value;
    supply.Id = null;
    this.supplyService.saveSupply(supply,"/Sanitario/save").subscribe(() => this._openSnackBar());
    this.cleanControl(this.sanitaryControls);
  }

  public saveSaleSupply(): void {
    let supply: SaleDTO = new SaleDTO();
    supply.Description = this.saleControls.get("description")?.value;
    supply.Name = this.saleControls.get("name")?.value;
    supply.SaleType = this.saleControls.get("type")?.value;
    supply.PesoSalePrice = this.saleControls.get("price")?.value;
    supply.Id = null;
    this.supplyService.saveSupply(supply,"/Venta/save").subscribe(() => this._openSnackBar());
    this.cleanControl(this.sanitaryControls);
  }

  private _openSnackBar(): void {
    this._snackBar.open("Insumo guardado", "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

}
