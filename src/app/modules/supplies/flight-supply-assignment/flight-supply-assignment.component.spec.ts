import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightSupplyAssignmentComponent } from './flight-supply-assignment.component';

describe('FlightSupplyAssignmentComponent', () => {
  let component: FlightSupplyAssignmentComponent;
  let fixture: ComponentFixture<FlightSupplyAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightSupplyAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightSupplyAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
