import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { CateringDTO, FlightDTO, PassengerDTO, SaleDTO, SanitaryDTO, SuppliesQuantitiesDTO, SupplyByFlightDTO, SupplyDTO } from 'src/app/core/model/common.class';
import { FlightService } from 'src/app/core/services/flight.service';
import { SupplyService } from 'src/app/core/services/supply.service';
import { PassengersListComponent } from '../passengers-list/passengers-list.component';

@Component({
  selector: 'app-flight-supply-assignment',
  templateUrl: './flight-supply-assignment.component.html',
  styleUrls: ['./flight-supply-assignment.component.css']
})
export class FlightSupplyAssignmentComponent implements OnInit {

  public loadingTable!: boolean;
  public controls!: FormGroup;
  public displayedColumns = ["name", "type", "quantity", "remove"];
  public dataSource!: MatTableDataSource<SupplyByFlightDTO>;
  public availableFlights!: FlightDTO[];
  public availableSupplies!: { catering: CateringDTO[], sale: SaleDTO[], sanitary: SanitaryDTO[] };
  public recommendedQuantities!: SuppliesQuantitiesDTO;
  public passengers!: PassengerDTO[];
  public loadingPassengers: boolean = true;
  public loadingRecommendedQuantities: boolean = true;
  public loadingFlights!: boolean;
  public readonly supplyTypeTexts: { [key: number]: string } = {
    0: "CATERING",
    1: "SANITARIO",
    2: "VENTA",
  };

  constructor(
    private flightService: FlightService,
    private supplyService: SupplyService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this._initializeProperties();
    this._setFlights();
    this._setSupplies();
    this._createControls();
  }

  private _setFlights(): void {
    this.loadingFlights = true;
    this.flightService.getFlights().subscribe(data =>{
      this.availableFlights = data;
      this.loadingFlights = false;
    });
  }

  private _createControls(): void {
    let controls = {
      flight: new FormControl(null, Validators.required),
      quantity: new FormControl({ value: null, disabled: true }, [Validators.required, Validators.min(1)]),
      supply: new FormControl({ value: null, disabled: true }, Validators.required),
    };
    this.controls = new FormGroup(controls);
  }

  private _setSuppliesRecommendedQuantities(): void {
    let flightId = this.controls.get("flight")?.value.FlightId
    this.supplyService.getSuppliesRecommendedQuantities(flightId).subscribe( data =>{
      this.recommendedQuantities = data;
      this.loadingRecommendedQuantities = false;
    });
  }

  private _initializeProperties(): void {
    this.availableSupplies = { catering: [], sale: [], sanitary: [] };
  }

  private _setSupplies(): void {
    this.supplyService.getCateringSupplies().subscribe(data => this.availableSupplies.catering = data );
    this.supplyService.getSanitarySupplies().subscribe(data => this.availableSupplies.sanitary = data);
    this.supplyService.getSaleSupplies().subscribe(data => this.availableSupplies.sale = data);
  }

  private _setSupplyDataSource(): void {
    let flightId = this.controls.get("flight")?.value?.FlightId
    this.supplyService.getSuppliesByFlight(flightId).subscribe(data => this._setDataSource(data));
  }

  private _setDataSource(supplies: SupplyByFlightDTO[]): void {
    this.loadingTable = false;
    this.dataSource = new MatTableDataSource(supplies);
  }

  private _setEnableControls(): void {
    this.controls.get("quantity")?.enable();
    this.controls.get("supply")?.enable();
  }

  private _getSupplyByFlightDto(): SupplyByFlightDTO {
    let supply: SupplyByFlightDTO = new SupplyByFlightDTO();
    supply.Id = null;
    supply.FlightId = this.controls.get("flight")?.value.FlightId;
    supply.FinalQuantity = this.controls.get("quantity")?.value;
    supply.InitialQuantity = this.controls.get("quantity")?.value;
    supply.Supply = this.controls.get("supply")?.value;
    supply.SupplyId = this.controls.get("supply")?.value.Id;
    return supply;
  }

  private _refreshView(): void {
    this.loadingTable = true;
    this._setSupplyDataSource();
  }

  private _setPassengers(): void {
    let flightId = this.controls.get("flight")?.value?.FlightId;
    this.supplyService.getPassengers(flightId).subscribe(data =>{
      this.passengers = data;
      this.loadingPassengers = false;
    });
  }

  public getSupplyTypeText(supply: any): string {
    if(supply.MenuType != undefined) {
      return this.supplyTypeTexts[0];
    } else if(supply.SanitaryType != undefined) {
      return this.supplyTypeTexts[1];
    } else {
      return this.supplyTypeTexts[2];
    }
  }

  public changeFlightLogic(): void {
    this.loadingTable = true;
    this._setPassengers();
    this._setSuppliesRecommendedQuantities();
    this._setSupplyDataSource();
    this._setEnableControls();
  }

  public invalidForms(): boolean {
    return this.controls.invalid;
  }

  public filterSupply(supplies: SupplyDTO[], type: string): SupplyDTO[] {
    let dataSourceSuppliesIds = this.dataSource?.data
    .filter(data => this.getSupplyTypeText(data.Supply) == type)
    .map(data => data.Supply.Id);
    return supplies?.filter(data => !dataSourceSuppliesIds?.includes(data.Id));
  }

  public doSave(): void {
    this.supplyService.saveSupplyByFlight(this._getSupplyByFlightDto()).subscribe(() => this._refreshView());
  }

  public cleanControl(): void {
    this.controls.get("quantity")?.reset();
    this.controls.get("supply")?.reset();
  }

  public delete(supply: SupplyByFlightDTO): void {
    this.supplyService.deleteSupplyByFlight(supply.Id).subscribe(() => this._refreshView())
  }

  public openDialog(): void {
    this.dialog.open(PassengersListComponent, {
      width: '800px',
      height: '800px',
      data: {
        passengers: this.passengers,
        recommendedQuantities: this.recommendedQuantities,
        flightName: this.controls.get("flight")?.value.Domain
      },
    });
  }

}
