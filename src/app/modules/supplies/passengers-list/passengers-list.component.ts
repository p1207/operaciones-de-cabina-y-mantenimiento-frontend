import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PassengerDTO, SuppliesQuantitiesDTO } from 'src/app/core/model/common.class';

@Component({
  selector: 'app-passengers-list',
  templateUrl: './passengers-list.component.html',
  styleUrls: ['./passengers-list.component.css']
})
export class PassengersListComponent implements OnInit {

  public displayedColumns = ["completeName", "seat", "condition", "diet", "class"];
  public dataSource!: MatTableDataSource<PassengerDTO>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { passengers: PassengerDTO[], recommendedQuantities: SuppliesQuantitiesDTO, flightName: string}) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.data.passengers);
  }

}
