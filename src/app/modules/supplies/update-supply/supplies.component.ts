import { Component, OnInit } from '@angular/core';
import { CateringDTO, SaleDTO, SanitaryDTO } from 'src/app/core/model/common.class';
import { SupplyService } from 'src/app/core/services/supply.service';

@Component({
  selector: 'app-supplies',
  templateUrl: './supplies.component.html',
  styleUrls: ['./supplies.component.css']
})
export class SuppliesComponent implements OnInit {

  public cateringSuppliesData!: CateringDTO[];
  public sanitarySuppliesData!: SanitaryDTO[];
  public saleSuppliesData!: SaleDTO[];
  public suppliesData!: any[];
  public auxiliaryFlightId = localStorage.getItem("flightId")!;
  public loading!: boolean;

  constructor(private supplyService: SupplyService) { }

  ngOnInit(): void {
    this._setData();
  }

  private _setData() : void {
    this._setSupplies();
  }

  private _setSupplies(): void {
    this.loading = true;
    this.supplyService.getSuppliesByFlight(this.auxiliaryFlightId).subscribe(data =>{
      this.loading = false;
      this.suppliesData = data;
      this.filterData();
    });
  }

  private filterData(): void {
    this._setCateringData();
    this._setSaleData();
    this._setSanitaryData();
  }

  private _setCateringData(): void {
    this.cateringSuppliesData = this.suppliesData.filter(supplyByFlight => supplyByFlight.Supply.MenuType != undefined);
  }

  private _setSaleData(): void {
    this.saleSuppliesData = this.suppliesData.filter(supplyByFlight => supplyByFlight.Supply.SaleType != undefined);
  }

  private _setSanitaryData(): void {
    this.sanitarySuppliesData = this.suppliesData.filter(supplyByFlight => supplyByFlight.Supply.SanitaryType != undefined);
  }

}
