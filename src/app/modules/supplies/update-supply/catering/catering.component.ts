import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { CateringDTO } from 'src/app/core/model/common.class';
import { FlightStatus } from 'src/app/core/model/enum.class';
import { FlightService } from 'src/app/core/services/flight.service';
import { SupplyService } from 'src/app/core/services/supply.service';

@Component({
  selector: 'app-catering',
  templateUrl: './catering.component.html',
  styleUrls: ['./catering.component.css']
})
export class CateringComponent {

  public displayedColumns: string[] = ['name', 'quantity', 'menuType', 'description', 'action'];
  public dataSource!: MatTableDataSource<CateringDTO>;
  @Input() public auxiliaryFlightId!: string;
  @Input() public cateringSuppliesData!: any[];

  public readonly menuTypeTexts: { [key: number]: string } = {
    0: "VEGANO",
    1: "VEGETARIANO",
    2: "CELIACO",
    3: "ESTANDAR"
  };

  constructor(
    private supplyService: SupplyService,
    private flightService: FlightService,
    private _snackBar: MatSnackBar) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.cateringSuppliesData = changes['cateringSuppliesData'].currentValue;
    this.dataSource = new MatTableDataSource(this.cateringSuppliesData);
  }

  public doSale(inputRef: any, supply: any): void {
    this.flightService.getFlightStatus(this.auxiliaryFlightId).subscribe(data => {
      if (data.Status == "en vuelo") {
        this._update(inputRef, supply);
      } else {
        this._openSnackBar(" Aún no se encuentra en vuelo ");
      }
    });
  }

  private _update(inputRef: any, supply: any): void {
    this._doSubtraction(inputRef, supply);
    this.supplyService.updateSupplyQuantity(supply).subscribe(() => {
      this._openSnackBar("Insumo entregado correctamente")
    });
  }

  private _doSubtraction(inputRef: any, supply: any): void {
    supply.FinalQuantity = supply.FinalQuantity - Number(inputRef.value);
    inputRef.value = "";
  }

  private _openSnackBar(message: string): void {
    this._snackBar.open(message, "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

}
