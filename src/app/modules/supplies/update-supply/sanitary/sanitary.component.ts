import { Component, Input, SimpleChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { FlightStatus } from 'src/app/core/model/enum.class';
import { FlightService } from 'src/app/core/services/flight.service';
import { SupplyService } from 'src/app/core/services/supply.service';

@Component({
  selector: 'app-sanitary',
  templateUrl: './sanitary.component.html',
  styleUrls: ['./sanitary.component.css']
})
export class SanitaryComponent {

  public displayedColumns: string[] = ['name','quantity','sanitaryType','description','action'];
  public dataSource!: MatTableDataSource<any>;
  @Input() public auxiliaryFlightId!: string;
  @Input() public sanitarySuppliesData!: any[];


  public readonly sanitaryTypeTexts: { [key: number]: string } = {
    0: "COMFORT",
    1: "LIMPIEZA",
    2: "DESCARTABLE",
  };

  constructor(
    private supplyService: SupplyService,
    private flightService: FlightService,
    private _snackBar: MatSnackBar) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.sanitarySuppliesData = changes['sanitarySuppliesData'].currentValue;
    this.dataSource = new MatTableDataSource(this.sanitarySuppliesData);
  }

  public doSale(inputRef: any, supply: any): void {
    this.flightService.getFlightStatus(this.auxiliaryFlightId).subscribe(data => {
      if (data.Status == "en vuelo") {
        this._update(inputRef, supply);
      } else {
        this._openSnackBar(" Aún no se encuentra en vuelo ");
      }
    });
  }

  private _update(inputRef: any, supply: any): void {
    this.supplyService.updateSupplyQuantity(supply).subscribe(() => {
      this._doSubtraction(inputRef, supply);
      this._openSnackBar("Insumo entregado correctamente")
    });
  }

  private _doSubtraction(inputRef: any, supply: any): void {
    supply.FinalQuantity = supply.FinalQuantity - Number(inputRef.value);
    inputRef.value = "";
  }

  private _openSnackBar(message: string): void {
    this._snackBar.open(message, "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

}
