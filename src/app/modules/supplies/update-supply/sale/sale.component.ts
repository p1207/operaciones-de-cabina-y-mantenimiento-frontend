import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { SaleDTO } from 'src/app/core/model/common.class';
import { FlightService } from 'src/app/core/services/flight.service';
import { SupplyService } from 'src/app/core/services/supply.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent {

  public showDollarCurrency: boolean = true;
  public displayedColumns: string[] = ['name', 'quantity', 'saleType', 'cost', 'description', 'action'];
  public dataSource!: MatTableDataSource<SaleDTO>;
  @Input() public saleSuppliesData!: any[];
  @Input() public auxiliaryFlightId!: string;

  public readonly saleTypeTexts: { [key: number]: string } = {
    0: "BEBIDA",
    1: "PERFUMERIA",
    2: "ELECTRONICA",
    3: "JOYERIA",
    4: "COMESTIBLE"
  };

  constructor(
    private supplyService: SupplyService,
    private flightService: FlightService,
    private _snackBar: MatSnackBar) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.saleSuppliesData = changes['saleSuppliesData'].currentValue;
    this.dataSource = new MatTableDataSource(this.saleSuppliesData);
  }

  public changeCurrency(): void {
    this.showDollarCurrency = !this.showDollarCurrency;
  }

  public doSale(inputRef: any, supply: any): void {
    this.flightService.getFlightStatus(this.auxiliaryFlightId).subscribe(data => {
      if (data.Status == "en vuelo") {
        this._update(inputRef, supply);
      } else {
        this._openSnackBar(" Aún no se encuentra en vuelo ");
      }
    });
  }

  private _update(inputRef: any, supply: any): void {
    this.supplyService.updateSupplyQuantity(supply).subscribe(() => {
      this._doSubtraction(inputRef, supply);
      this._openSnackBar("Insumo entregado correctamente")
    });
  }

  private _doSubtraction(inputRef: any, supply: any): void {
    supply.FinalQuantity = supply.FinalQuantity - Number(inputRef.value);
    inputRef.value = "";
  }

  private _openSnackBar(message: string): void {
    this._snackBar.open(message, "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }


}
