import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ControlDTO } from 'src/app/core/model/common.class';
import { jsPDF } from "jspdf";

@Component({
  selector: 'app-control-view',
  templateUrl: './control-view.component.html',
  styleUrls: ['./control-view.component.css']
})
export class ControlViewComponent implements OnInit {

  @ViewChild('content', {static : false}) htmlContent!: ElementRef;

  constructor(
    private dialogRef: MatDialogRef<ControlViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { control: ControlDTO, flightCode: string, observation: string }
  ) { }

  ngOnInit(): void { }

  public makePdf(): void {
    const doc = new jsPDF('p','pt','a4');
    doc.html(this.htmlContent.nativeElement, {
      callback: (doc) => doc.save("control.pdf")
    });
  }

}
