import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ControlDTO, ControlSupplyDTO, ControlTaskDTO, RoleDTO } from 'src/app/core/model/common.class';
import { MaintenanceSupplyService } from 'src/app/core/services/maintenance-supply.service';
import { RoleService } from 'src/app/core/services/role.service';

@Component({
  selector: 'app-add-control',
  templateUrl: './add-control.component.html',
  styleUrls: ['./add-control.component.css']
})
export class AddControlComponent implements OnInit {

  public taskDataSource!: MatTableDataSource<ControlTaskDTO>;
  public suppliesDataSource!: MatTableDataSource<ControlSupplyDTO>;
  public availableSupplies!: ControlSupplyDTO[];
  public availableRoles!: RoleDTO[];
  public supplyDisplayedColumns = ["description", "remove"];
  public taskDisplayedColumns = ["description", "role", "remove"];
  public controls!: FormGroup;
  public taskControl!: FormGroup;
  public supplyControl!: FormControl;
  public tabIndex: number = 0;
  public title!: string;
  public availableTypes: string[] = ["RUTINARIO", "EVENTUAL"];
  public readonly controlTypeTexts: { [key: number]: string } = {
    0: "RUTINARIO",
    1: "EVENTUAL",
  };

  constructor(
    private roleService: RoleService,
    private supplyService: MaintenanceSupplyService,
    private dialogRef: MatDialogRef<AddControlComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { control: ControlDTO, title: string, type: number, fligth: number},
    ) { }

  ngOnInit(): void {
    this._setSupplies();
    this._setRoles();
    this._createControls();
    this._setDataSource();
    this._setTitle();
  }

  private _setSupplies(): void {
    this.supplyService.getSupplies().subscribe(data => this.availableSupplies = data);
  }

  private _setRoles(): void {
    this.roleService.getRoles().subscribe(data => this.availableRoles = data);
  }

  private _createControls(): void {
    let controls = {
      role: new FormControl(this.data?.control?.Role, Validators.required),
      description: new FormControl(this.data?.control?.Description, Validators.required),
      name: new FormControl(this.data?.control?.Name, Validators.required),
      type: new FormControl(this.data?.control?.Type),
    }
    let taskControls = {
      description: new FormControl(null, Validators.required),
      role: new FormControl(null, Validators.required),
    }
    this.controls = new FormGroup(controls);
    this.taskControl = new FormGroup(taskControls);
    this.supplyControl = new FormControl(null, Validators.required);
  }

  private _setDataSource(): void {
    let taskList: ControlTaskDTO[] = this.data.control != undefined ? this.data.control.Tasks : [];
    let suppliesList: ControlSupplyDTO[] = this.data.control != undefined ? this.data.control.Supplies : [];
    this.taskDataSource = new MatTableDataSource(taskList);
    this.suppliesDataSource = new MatTableDataSource(suppliesList);
  }

  private _setTitle(): void {
    this.title = this.data.title;
  }

  public addSupply(): void {
    let supply: ControlSupplyDTO = this.supplyControl.value;
    this.suppliesDataSource.data.push(supply);
    this.suppliesDataSource._updateChangeSubscription();
    this.supplyControl?.reset();
  }

  public add(): void {
    if (this.tabIndex == 0) {
      this.addTask();
    } else {
      this.addSupply();
    }
  }

  public addTask(): void {
    let task: ControlTaskDTO = { Id: null, Description: this.taskControl.get("description")?.value,Role:this.taskControl.get("role")?.value, RoleId : this.taskControl.get("role")?.value.Id};
    this.taskDataSource.data.push(task);
    this.taskDataSource._updateChangeSubscription();
    this.taskControl?.reset();
  }

  public disableSaveButton(): boolean | undefined {
    return this.invalidFoms() || this.emptyTaskList() || this.emptySuppliesList();
  }

  public disableButton(): boolean {
    if (this.tabIndex == 0)
      return this.taskControl.invalid || this.duplicateTask();
    return this.supplyControl.invalid || this.duplicateSupply();
  }

  public deleteTask(task: ControlTaskDTO): void {
    const index = this.taskDataSource.data.indexOf(task);
    this.taskDataSource.data.splice(index, 1);
    this.taskDataSource._updateChangeSubscription();
  }

  public deleteSupply(supply: ControlSupplyDTO): void {
    const index = this.suppliesDataSource.data.indexOf(supply);
    this.suppliesDataSource.data.splice(index, 1);
    this.suppliesDataSource._updateChangeSubscription();
  }

  public getTooltipMessage(): string {
    const emptyTask = "Debe agregar al menos una tarea para poder guardar";
    const emptySupplies = "Debe agregar almenos un insumo";
    const invalidControlsMessage = "Debe rellenar los campos obligatorios";
    let res: string = "";
    if (this.emptyTaskList())
      res = emptyTask;
    if (this.emptySuppliesList())
      res = emptySupplies;
    if (this.invalidFoms())
      res = invalidControlsMessage;
    return res;
  }

  public emptyTaskList(): boolean {
    return this.taskDataSource.data.length == 0;
  }

  public emptySuppliesList(): boolean {
    return this.suppliesDataSource.data.length == 0;
  }

  public invalidFoms(): boolean {
    return this.controls.invalid;
  }

  public getControl(): ControlDTO {
    let control: ControlDTO = new ControlDTO();
    control.Id = this.data.control == null ? null : this.data.control.Id;
    control.Role = this.controls.get("role")?.value;
    control.Description = this.controls.get("description")?.value;
    control.Name = this.controls.get("name")?.value;
    control.Tasks = this.taskDataSource.data;
    control.Supplies = this.suppliesDataSource.data;
    control.Type = this.controlTypeTexts[this.data.type];
    control.FligthId = this.data.fligth;
    return control;
  }

  public closeAndSave(): void {
    this.dialogRef.close(this.getControl());
  }

  public duplicateTask(): boolean {
    return this.taskDataSource.data.some(task => task.Description == this.taskControl.get("description")?.value);
  }

  public duplicateSupply(): boolean {
    return this.suppliesDataSource.data.some(supply => supply.Id == this.supplyControl.value);
  }

}
