import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightControlAssignmentComponent } from './flight-control-assignment.component';

describe('FlightControlAssignmentComponent', () => {
  let component: FlightControlAssignmentComponent;
  let fixture: ComponentFixture<FlightControlAssignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightControlAssignmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightControlAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
