import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { ControlByFlightDTO, ControlDTO, FlightDTO } from 'src/app/core/model/common.class';
import { ControlService } from 'src/app/core/services/control.service';
import { FlightService } from 'src/app/core/services/flight.service';
import { AddControlComponent } from '../add-control/add-control.component';
import { ControlViewComponent } from '../control-view/control-view.component';

@Component({
  selector: 'app-flight-control-assignment',
  templateUrl: './flight-control-assignment.component.html',
  styleUrls: ['./flight-control-assignment.component.css']
})
export class FlightControlAssignmentComponent implements OnInit {

  public controlsByFlightData!: ControlByFlightDTO[];
  public controlsRoutinData!: ControlDTO[];
  public controlsData!: ControlDTO[];
  public loadingTable!: boolean;
  public availableFlights!: FlightDTO[];
  public controls!: FormGroup;
  public displayedColumns = ["name", "type", "role", "controlView", "remove"];
  public dataSource!: MatTableDataSource<ControlByFlightDTO>;
  public loadingFlights: boolean = true;
  public filteredOptions!: Observable<string[]>;


  constructor(
    private flightService: FlightService,
    private controlService: ControlService,
    public dialog: MatDialog ) { }

  ngOnInit(): void {
    this._setFlights();
    this._createControls();
    this._setControlData();
  }

  private _setFlights(): void {
    this.flightService.getFlights().subscribe(data => {
      this.availableFlights = data
      this.loadingFlights = false;
    });
  }

  private _createControls(): void {
    let controls = {
      flight: new FormControl(null, Validators.required),
      observation: new FormControl({ value: null, disabled: true }, Validators.required),
      control: new FormControl({ value: null, disabled: true }, Validators.required),
    };
    this.controls = new FormGroup(controls);
  }

  public openDialogControl(title: string, control?: ControlDTO): void {
    const dialogRef = this.dialog.open(AddControlComponent, {
      width: '700px',
      height: '900px',
      data!: {control: control, title: title, type: 1, fligth: this.controls.get("flight")?.value.FlightId },
    });

    dialogRef.afterClosed().subscribe(control => {
      if(control){
        this.controlService.saveControl(control).subscribe(() =>{
          this._refreshView();
        });
      }
    });
  }

  private _getControlByFlightDto(): ControlByFlightDTO {
    let control: ControlByFlightDTO = new ControlByFlightDTO();
    control.ControlId = this.controls.get("control")?.value.Id;
    control.FlightId = this.controls.get("flight")?.value.FlightId;
    control.Observation = this.controls.get("observation")?.value;
    control.CreationDate = new Date(Date.now());
    control.Id = null;
    return control;
  }

  private _refreshView(): void {
    this.loadingTable = true;
    this._setControlDataSourceAfterRefresh();
  }

  private _setControlData(): void {
    this.controlService.getControls().subscribe(data =>{
      this.controlsRoutinData = data.filter( c=> c.Type == "RUTINARIO");
      this.controlsData = data;
    });
  }

  private _setDataSource(controls: ControlByFlightDTO[]): void {
    this.loadingTable = false;
    this.dataSource = new MatTableDataSource(controls);
    this.dataSource._updateChangeSubscription();
  }

  private _setEnableControls(): void {
    this.controls.get("observation")?.enable();
    this.controls.get("control")?.enable();
  }

  private _setControlDataSourceAfterRefresh(): void {
    let flightId = this.controls.get("flight")?.value?.FlightId
    this.controlService.getControlsByFlight(flightId).subscribe(data => {
      this.controlsByFlightData = data.AllControls;
      this._setDataSource(this.controlsByFlightData);
    });
  }

  public getFlightById(flightId: number): FlightDTO | undefined {
    return this.availableFlights.find(flight => flight.FlightId == flightId);
  }

  public changeFlightLogic(): void {
    this.loadingTable = true;
    this._setControlDataSourceAfterRefresh();
    this._setEnableControls();
  }

  public invalidForms(): boolean {
    return this.controls.invalid;
  }

  public cleanControl(): void {
    this.controls.get("observation")?.reset();
    this.controls.get("control")?.reset();
  }

  public filterAvailableControls(controls: ControlDTO[]): ControlDTO[] {
    let dataSourceControlIds = this.dataSource?.data.map(data => data.ControlId);
    return controls?.filter(data => dataSourceControlIds?.includes(data.Id!) == false);
  }

  public doSave(): void {
    this.controlService.saveControlByFlight(this._getControlByFlightDto()).subscribe(() => this._refreshView());
  }

  public doDelete(control: ControlByFlightDTO): void {
    this.controlService.deleteControlByFlight(control.Id).subscribe(() => this._refreshView());
  }

  public openDialog(control: ControlDTO | undefined, flighCode: string | undefined, observation: string | undefined): void {
    this.dialog.open(ControlViewComponent, {
      width: '1200px',
      height: '600px',
      data: {control: control, flightCode: flighCode, observation: observation},
    });
  }


}
