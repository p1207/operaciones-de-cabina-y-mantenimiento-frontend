import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ControlDTO } from 'src/app/core/model/common.class';
import { ControlService } from 'src/app/core/services/control.service';
import { AddControlComponent } from '../add-control/add-control.component';

@Component({
  selector: 'app-control-configutarions',
  templateUrl: './control-configutarions.component.html',
  styleUrls: ['./control-configutarions.component.css']
})
export class ControlConfigutarionsComponent implements OnInit {

  public dataSource!: MatTableDataSource<ControlDTO>;
  public controlsData!: ControlDTO[];
  public loading!: boolean;

  public pageSize = 5;
  public pageNumber = 1;
  public pageSizeOptions = [5,10,20,50];

  constructor(public dialog: MatDialog,
    private controlService: ControlService ) { }

  ngOnInit(): void {
    this._setData();
  }

  private _setData(): void {
    this._setLoading(true);
    this.controlService.getControls().subscribe(data =>{
      this.controlsData = data;
      this.controlsData = this.controlsData.filter(c => c.Type == "RUTINARIO")
      this.dataSource = new MatTableDataSource(this.controlsData);
      this._setLoading(false);
    });
  }

  private _refreshView(): void {
    this.ngOnInit();
  }

  private _setLoading(value: boolean): void {
    this.loading = value;
  }

  public openDialog(title: string, control?: ControlDTO): void {
    const dialogRef = this.dialog.open(AddControlComponent, {
      width: '700px',
      height: '900px',
      data!: {control: control, title: title, type: 0},
    });

    dialogRef.afterClosed().subscribe(control => {
      if(control){
        this.controlService.saveControl(control).subscribe(() => this._refreshView());
      }
    });
  }

  public handlePage(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.controlsData = this.dataSource.filteredData;
  }

  public delete(control: ControlDTO): void{
    const index = this.controlsData.indexOf(control, 0);
    this.controlsData.splice(index, 1);
    this.controlService.deleteControl(control.Id).subscribe();
  }

}
