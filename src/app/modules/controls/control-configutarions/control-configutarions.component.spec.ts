import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlConfigutarionsComponent } from './control-configutarions.component';

describe('ControlConfigutarionsComponent', () => {
  let component: ControlConfigutarionsComponent;
  let fixture: ComponentFixture<ControlConfigutarionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlConfigutarionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlConfigutarionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
