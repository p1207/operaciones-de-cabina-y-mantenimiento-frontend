import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ControlByFlightDTO, ControlDTO, FlightDTO } from 'src/app/core/model/common.class';
import { ControlService } from 'src/app/core/services/control.service';
import { FlightService } from 'src/app/core/services/flight.service';
import { ControlViewComponent } from '../control-view/control-view.component';

@Component({
  selector: 'app-update-control',
  templateUrl: './update-control.component.html',
  styleUrls: ['./update-control.component.css']
})
export class UpdateControlComponent implements OnInit {

  public controlsByFlightData!: ControlByFlightDTO[];
  public controlsData!: ControlDTO[];
  public loadingTable!: boolean;
  public availableFlights!: FlightDTO[];
  public controls!: FormGroup;
  public displayedColumns = ["name", "type", "status", "controlView"];
  public dataSource!: MatTableDataSource<ControlByFlightDTO>;
  public selectedControl!: ControlDTO | undefined;
  public selectedControlByFlight!: ControlByFlightDTO | undefined;
  public loadingFlights!: boolean;

  constructor(
    private flightService: FlightService,
    private controlService: ControlService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this._setFlights();
    this._createControls();
    this._setControlData();
  }

  private _setFlights(): void {
    this.loadingFlights = true;
    this.flightService.getFlights().subscribe(data =>{
      this.availableFlights = data;
      this.loadingFlights = false;
    });
  }

  private _createControls(): void {
    let controls = {
      flight: new FormControl(null, Validators.required),
      observation: new FormControl({ value: null, disabled: true }, Validators.required),
    };
    this.controls = new FormGroup(controls);
  }

  private _setControlData(): void {
    this.controlService.getControls().subscribe(data => this.controlsData = data);
  }

  private _setControlDataSourceAfterRefresh(): void {
    let flightId = this.controls.get("flight")?.value?.FlightId
    this.controlService.getControlsByFlight(flightId).subscribe(data => {
      this.controlsByFlightData = data.AllControls;
      this._setDataSource(this.controlsByFlightData);
    });
  }

  private _setDataSource(controls: ControlByFlightDTO[]): void {
    this.loadingTable = false;
    this.dataSource = new MatTableDataSource(controls);
    this.dataSource._updateChangeSubscription();
  }

  private _setEnableControls(): void {
    this.controls.get("observation")?.enable();
  }

  public getFlightById(flightId: number): FlightDTO | undefined {
    return this.availableFlights.find(flight => flight.FlightId == flightId);
  }

  public changeFlightLogic(): void {
    this.loadingTable = true;
    this._setControlDataSourceAfterRefresh();
    this._setEnableControls();
  }

  public cleanControl(): void {
    this.controls.get("observation")?.reset();
  }

  public doSave(): void {
    this.selectedControlByFlight!.Status = "CHEQUEADO";
    this.selectedControlByFlight!.Observation = this.controls.get("observation")?.value;
    this.controlService.updateControlByFlight(this.selectedControlByFlight).subscribe(() => this._refreshView());
  }

  public invalidForms(): boolean {
    return this.controls.invalid;
  }

  public getCurrentDate(): Date {
    return new Date(Date.now());
  }

  public getCurrentUser(): string | null {
    return localStorage.getItem("name");
  }

  public openDialog(control: ControlDTO | undefined, flighCode: string | undefined, observation: string | undefined): void {
    this.dialog.open(ControlViewComponent, {
      width: '1200px',
      height: '600px',
      data: {control: control, flightCode: flighCode, observation: observation},
    });
  }

  public isNotSelectedControl(): boolean {
    return this.selectedControlByFlight == undefined;
  }

  public trySetSelectedControl(control: ControlByFlightDTO) {
    if(control.Status == "CHEQUEADO") {
      this._openSnackBar();
    } else {
      this.selectedControl = control?.Control;
      this.selectedControlByFlight = control;
    }
  }

  private _refreshView(): void {
    this._setControlDataSourceAfterRefresh();
    this.selectedControlByFlight = undefined;
    this.selectedControl = undefined;
  }

  private _openSnackBar(): void {
    this._snackBar.open("Los controles chequeados no pueden cambiarse de estado", "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }








}
