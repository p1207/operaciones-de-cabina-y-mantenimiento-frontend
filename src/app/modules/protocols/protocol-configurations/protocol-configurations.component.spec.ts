import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtocolConfigurationsComponent } from './protocol-configurations.component';

describe('ProtocolConfigurationsComponent', () => {
  let component: ProtocolConfigurationsComponent;
  let fixture: ComponentFixture<ProtocolConfigurationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProtocolConfigurationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtocolConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
