import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProtocolDTO, ProtocolTaskDTO } from 'src/app/core/model/common.class';
import { ProtocolService } from 'src/app/core/services/protocol.service';

@Component({
  selector: 'app-protocol-configurations',
  templateUrl: './protocol-configurations.component.html',
  styleUrls: ['./protocol-configurations.component.css']
})
export class ProtocolConfigurationsComponent implements OnInit {

  public protocol!: ProtocolDTO;
  public controls!: FormGroup;
  public protocolLastId: string = "1";

  constructor(
    private protocolService: ProtocolService,
    private _snackBar: MatSnackBar) {
    this.protocol= new ProtocolDTO;
    this.protocol.Tasks = [];
  }

  ngOnInit(): void {
    this._createControls();
  }

  public addProtocolTask(): void {
    let protocolTask = new ProtocolTaskDTO();
    protocolTask.Id = Number(this.protocolLastId);
    protocolTask.Name = this.controls.get("protocolTask")?.value;
    this.protocol.Tasks.push(protocolTask);
    this.protocolLastId = (Number(this.protocolLastId) + 1).toString();
    this.controls.get("protocolTask")?.reset();
  }

  private _createControls(): void {
    let controls = {
      protocolTask: new FormControl(null, Validators.required),
      protocolName: new FormControl(null, Validators.required),
    };
    this.controls = new FormGroup(controls);
  }

  public doSave(): void {
    this.protocol.Name = this.controls.get("protocolName")?.value;
    this.protocolService.saveProtocol(this.protocol).subscribe(() =>{
      this.doReset();
      this._openSnackBar();
    });
  }

  private _openSnackBar(): void {
    this._snackBar.open("Protocolo guardado", "Ok!", {
      duration: 2000,
      panelClass: ['blue-snackbar']
    });
  }

  public doReset(): void {
    this.protocol.Tasks = [];
  }

}
