import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CateringSuppliesReportComponent } from './catering-supplies-report.component';

describe('CateringSuppliesReportComponent', () => {
  let component: CateringSuppliesReportComponent;
  let fixture: ComponentFixture<CateringSuppliesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CateringSuppliesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CateringSuppliesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
