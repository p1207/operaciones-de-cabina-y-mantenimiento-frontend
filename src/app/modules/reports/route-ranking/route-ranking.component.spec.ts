import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteRankingComponent } from './route-ranking.component';

describe('RouteRankingComponent', () => {
  let component: RouteRankingComponent;
  let fixture: ComponentFixture<RouteRankingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouteRankingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
