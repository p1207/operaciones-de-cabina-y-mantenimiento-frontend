import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannedVsLandedComponent } from './planned-vs-landed.component';

describe('PlannedVsLandedComponent', () => {
  let component: PlannedVsLandedComponent;
  let fixture: ComponentFixture<PlannedVsLandedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlannedVsLandedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannedVsLandedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
