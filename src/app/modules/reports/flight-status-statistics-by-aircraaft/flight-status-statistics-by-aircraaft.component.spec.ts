import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightStatusStatisticsByAircraaftComponent } from './flight-status-statistics-by-aircraaft.component';

describe('FlightStatusStatisticsByAircraaftComponent', () => {
  let component: FlightStatusStatisticsByAircraaftComponent;
  let fixture: ComponentFixture<FlightStatusStatisticsByAircraaftComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightStatusStatisticsByAircraaftComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightStatusStatisticsByAircraaftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
