import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleSuppliesReportComponent } from './sale-supplies-report.component';

describe('SaleSuppliesReportComponent', () => {
  let component: SaleSuppliesReportComponent;
  let fixture: ComponentFixture<SaleSuppliesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaleSuppliesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleSuppliesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
