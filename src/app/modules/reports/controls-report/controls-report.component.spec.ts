import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlsReportComponent } from './controls-report.component';

describe('ControlsReportComponent', () => {
  let component: ControlsReportComponent;
  let fixture: ComponentFixture<ControlsReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlsReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
