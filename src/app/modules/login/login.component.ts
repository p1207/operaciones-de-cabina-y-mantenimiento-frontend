import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserDTO } from 'src/app/core/model/common.class';
import { LoginService } from 'src/app/core/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public controls!: FormGroup;
  public readonly statusTexts: { [key: string]: string } = {
    "Técnico" : "TECHNICIAN",
    "Supervisor" : "SUPERVISOR",
    "Auxiliar" : "AUXILIARY",
    "Copiloto" : "COPILOT",
    "Piloto" : "PILOT"
  };

  public showError!: boolean;
  public loading!: boolean;

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
    localStorage.clear();
    let controls = {
      name: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    }
    this.controls = new FormGroup(controls);
  }

  private _getUserDto(): UserDTO {
    let user = new UserDTO();
    user.Password = this.controls.get("password")?.value;
    user.Username = this.controls.get("name")?.value;
    return user;
  }

  public tryLogin(): void {
    this._setShowError(false);
    this._setLading(true);
    this.loginService.tryLogin(this._getUserDto())
    .subscribe(data => this._loginLogic(data));
  }
  private _loginLogic(user: UserDTO) {
    this._setLading(false);
    if (user.Name != null){
      this._doSuccesfullLogic(user);
    } else {
      this._setShowError(true);
    }
  }

  private _doSuccesfullLogic(user: UserDTO): void {
    this._setLocalStorageData(user);
    this._redirectToHome();
  }

  private _redirectToHome() {
    this.router.navigate(["/home"]);
  }

  private _setShowError(value: boolean) {
    this.showError = value;
  }

  private _setLocalStorageData(user: UserDTO) {
    localStorage.setItem("profile", this.statusTexts[user.Role.Name]);
    localStorage.setItem("userName", user.Username)
    localStorage.setItem("name", user.Name);
    localStorage.setItem("surname", user.Surname);
    localStorage.setItem("userId", user.Id.toString());
    localStorage.setItem("email", user.Email);
    localStorage.setItem("abilities", JSON.stringify(user.Abilities))
    if(user.CrewId){
      let id = {idpersonal_fk : user.CrewId}
      this.loginService.getFlight(id).subscribe(data =>{
        let flightTakenOff = data.find(flight => flight.Status == 5)?.FlightId;
        if(flightTakenOff){
          localStorage.setItem("flightId", flightTakenOff);
        }
        localStorage.setItem("assignedflights", JSON.stringify(data));
      });
    }
  }

  private _setLading(value: boolean): void {
    this.loading = value;
  }
}

