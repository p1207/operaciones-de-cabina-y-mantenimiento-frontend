import { Component, OnInit } from '@angular/core';
import { AuditoryDTO } from 'src/app/core/model/common.class';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { AuditService } from 'src/app/core/services/audits.service';

@Component({
  selector: 'app-audits',
  templateUrl: './audits.component.html',
  styleUrls: ['./audits.component.css']
})
export class AuditsComponent implements OnInit {

  public audits!: AuditoryDTO[];

  constructor(
    private auditsService: AuditService
  ) { }

  ngOnInit(): void {
    this.getAudits();
  }

  public dowloadCSV(): void {
    new ngxCsv(this.audits, "Auditoria", this._getOptions())
  }

  private _getOptions() {
    return {
      fieldSeparator: ' ; ',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Auditorias',
      useBom: true,
      headers: ["Id", " Date"," User"," Ip", ]
    };
  }

  private getAudits(){
    this.auditsService.getAll().subscribe(data => this.audits = data );
  }

}
