import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { NotificationDTO } from 'src/app/core/model/common.class';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  public notificationsData: NotificationDTO[] = [];
  public loading!: boolean;
  public pageSize = 5;
  public pageNumber = 1;
  public pageSizeOptions = [5,10,20,50];
  public userId: string = localStorage.getItem("userId")!;

  constructor(private notificationsService: NotificationService) { }

  ngOnInit(): void {
    this._setNotifications();
  }

  private _setNotifications() {
    this.setLoading(true);
    this.notificationsService.getNotifications(this.userId).subscribe(data =>{
      this.notificationsData = data;
      this.setLoading(false);
    });
  }

  private setLoading(value: boolean): void {
    this.loading = value;
  }

  public handlePage(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
  }

  public getNgClassObject(notification: NotificationDTO) {
    return { 'alert-font-color' : notification.Priority == 'ALTA', 'normal-font-color': notification.Priority != 'ALTA'}
  }

  public doDelete(notification: NotificationDTO): void {
    this.notificationsService.deleteNotification(notification.Id.toString()).subscribe(() => this.ngOnInit());
  }

}
