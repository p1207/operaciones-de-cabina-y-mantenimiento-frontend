import { Component, OnInit } from '@angular/core';
import { FlightByCrewMember, UserDTO } from 'src/app/core/model/common.class';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  public user!: UserDTO;
  public roleName!: string;
  public assignedFlights: FlightByCrewMember[] = [];
  public readonly statusTexts: { [key: string]: string } = {
    "TECHNICIAN" : "Técnico",
    "SUPERVISOR" : "Supervisor",
    "AUXILIARY" : "Auxiliar"
  };

  public readonly flightStatusText: { [key: number]: string } = {
    1 : "programado",
    2 : "confirmado",
    3 : "pre-embarque",
    4 : "despegado",
    5 : "en vuelo",
    6 : "aterrizado",
    7 : "finalizado",
    8 : "demorado",
    9 : "re-programado",
    10 : "cancelado"
  };



  constructor() { }

  ngOnInit(): void {
    this._getLocalStorageInfo();
  }

  private _getLocalStorageInfo() {
    this.user = new UserDTO();
    this.roleName = this.statusTexts[localStorage.getItem("profile")!];
    this.user.Username = localStorage.getItem("userName")!;
    this.user.Name = localStorage.getItem("name")!;
    this.user.Surname = localStorage.getItem("surname")!;
    this.user.FlightId = localStorage.getItem("flightId")!;
    this.user.Email = localStorage.getItem("email")!;
    this.user.Abilities = JSON.parse(localStorage.getItem("abilities")!);
    this.assignedFlights = JSON.parse(localStorage.getItem("assignedflights")!) ?? [];
  }

  public getBeautyAbilities(): string {
    return this.user.Abilities.map(ability => ability.Name).join(", ");
  }

  public getBeautyFlights(): string {
    return this.assignedFlights.map(data => data.FlightId + " - " + this.flightStatusText[data.Status]).join(", ")
  }



}
