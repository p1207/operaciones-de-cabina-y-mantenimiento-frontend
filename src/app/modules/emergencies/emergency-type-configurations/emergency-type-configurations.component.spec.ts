import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyTypeConfigurationsComponent } from './emergency-type-configurations.component';

describe('EmergencyConfigurationsComponent', () => {
  let component: EmergencyTypeConfigurationsComponent;
  let fixture: ComponentFixture<EmergencyTypeConfigurationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergencyTypeConfigurationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyTypeConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
