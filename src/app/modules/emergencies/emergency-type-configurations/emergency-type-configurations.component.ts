import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { EmergencyTypeDTO } from 'src/app/core/model/common.class';
import { EmergencyTypeService } from 'src/app/core/services/emergency-type.service';
import { AddEmergencyTypeComponent } from '../add-emergency-type/add-emergency-type.component';

@Component({
  selector: 'app-emergency-type-configurations',
  templateUrl: './emergency-type-configurations.component.html',
  styleUrls: ['./emergency-type-configurations.component.css']
})
export class EmergencyTypeConfigurationsComponent implements OnInit {

  public emergencyData!: EmergencyTypeDTO[];
  public loading!: boolean;

  public pageSize = 5;
  public pageNumber = 1;
  public pageSizeOptions = [5,10,20,50];

  constructor(
    public dialog: MatDialog,
    private emergencyTypeService: EmergencyTypeService) { }

  ngOnInit(): void {
    this._setData();
  }

  private _setData(): void {
    this._setLoading(true);
    this.emergencyTypeService.getEmergenciesType().subscribe(data =>{
      this.emergencyData = data;
      this._setLoading(false);
    });
  }

  private _setLoading(value: boolean): void {
    this.loading = value;
  }

  public handlePage(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
  }

  public getMargin(taskId: number): { [klass: string]: any; } | null {
    let stringIdlenght: number = taskId.toString().length;
    let marginValue: number = 60*stringIdlenght;
    return {'margin-left': marginValue +'px'}
  }

  public openDialog(title: string, emergencyType?: EmergencyTypeDTO): void {
    const dialogRef = this.dialog.open(AddEmergencyTypeComponent, {
      width: '700px',
      height: '700px',
      data: {emergencyType: emergencyType, title: title},
    });

    dialogRef.afterClosed().subscribe(emergencyTypeResponse => {
      if(emergencyTypeResponse){
        this.emergencyTypeService.saveEmergencyType(emergencyTypeResponse).subscribe(() => this._refreshView());
      }
    });
  }

  public delete(emergency: EmergencyTypeDTO): void{
    const index = this.emergencyData.indexOf(emergency, 0);
    this.emergencyData.splice(index, 1);
    this.emergencyTypeService.deleteEmergencyType(emergency.Id).subscribe(() => this._refreshView());
  }

  private _refreshView(): void {
    this.ngOnInit();
  }

}
