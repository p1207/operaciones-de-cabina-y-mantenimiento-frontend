import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { EmergencyDTO } from 'src/app/core/model/common.class';
import { EmergencyService } from 'src/app/core/services/emergency.service';
import { AddEmergencyComponent } from '../add-emergency/add-emergency.component';

@Component({
  selector: 'app-emergency',
  templateUrl: './emergency.component.html',
  styleUrls: ['./emergency.component.css']
})
export class EmergencyComponent implements OnInit {

  public emergencyData!: EmergencyDTO[];
  public loading!: boolean;

  public pageSize = 5;
  public pageNumber = 1;
  public pageSizeOptions = [5,10,20,50];

  constructor(
    public dialog: MatDialog,
    private emergencyService: EmergencyService) { }

  ngOnInit(): void {
    this._setData();
  }

  private _setData(): void {

    this._setLoading(true);
    this.emergencyService.getEmergencies().subscribe(data =>{
      this.emergencyData = data;
      this._setLoading(false);
    });
  }

  private _setLoading(value: boolean): void {
    this.loading = value;
  }

  public handlePage(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.pageNumber = event.pageIndex + 1;
  }

  public getMargin(taskId: number): { [klass: string]: any; } | null {
    let stringIdlenght: number = taskId.toString().length;
    let marginValue: number = 60*stringIdlenght;
    return {'margin-left': marginValue +'px'}
  }

  public openDialog(title: string, emergency?: EmergencyDTO): void {
    const dialogRef = this.dialog.open(AddEmergencyComponent, {
      width: '700px',
      height: '700px',
      data: {emergency: emergency, title: title},
    });

    dialogRef.afterClosed().subscribe(emergencyResponse => {
      if(emergencyResponse){
         this.emergencyService.saveEmergency(emergencyResponse).subscribe(() => this._refreshView());
      }
    });
  }

  private _refreshView(): void {
    this.ngOnInit();
  }
}
