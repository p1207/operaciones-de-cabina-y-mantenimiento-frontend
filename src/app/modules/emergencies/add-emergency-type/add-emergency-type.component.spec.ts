import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEmergencyTypeComponent } from './add-emergency-type.component';

describe('AddEmergencyTypeComponent', () => {
  let component: AddEmergencyTypeComponent;
  let fixture: ComponentFixture<AddEmergencyTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEmergencyTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEmergencyTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
