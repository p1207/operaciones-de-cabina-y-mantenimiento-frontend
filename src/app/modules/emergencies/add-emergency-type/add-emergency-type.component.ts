import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmergencyTypeDTO, ProtocolDTO } from 'src/app/core/model/common.class';
import { ProtocolService } from 'src/app/core/services/protocol.service';

@Component({
  selector: 'app-add-emergency-type',
  templateUrl: './add-emergency-type.component.html',
  styleUrls: ['./add-emergency-type.component.css']
})
export class AddEmergencyTypeComponent implements OnInit {
  public controls!: FormGroup;
  public title!: string;
  public availableProtocols!: ProtocolDTO[];

  constructor(
    private protocolService: ProtocolService,
    private dialogRef: MatDialogRef<AddEmergencyTypeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { emergencyType: EmergencyTypeDTO, title: string },
  ) { }

  ngOnInit(): void {
    this._setProtocols();
    this._createControls();
    this._setTitle();
  }

  private _setTitle() {
    this.title = this.data.title;
  }

  private _createControls() {
    let controls = {
      emergencyTypeName: new FormControl(this.data?.emergencyType?.Name, Validators.required),
      protocol: new FormControl(this.data?.emergencyType?.Protocol?.Id, Validators.required),
    }
    this.controls = new FormGroup(controls);
  }

  private _setProtocols() {
    this.protocolService.getProtocols().subscribe(data => this.availableProtocols = data);
  }

  public getMargin(taskId: number): { [klass: string]: any; } | null {
    let stringIdlenght: number = taskId.toString().length;
    let marginValue: number = 60*stringIdlenght;
    return {'margin-left': marginValue +'px'}
  }

  public closeAndSave(): void {
    this.dialogRef.close(this._getEmergencyTypeDTO());
  }

  private _getEmergencyTypeDTO(): EmergencyTypeDTO {
    let emergency: EmergencyTypeDTO = new EmergencyTypeDTO;
    emergency.Id = this.data.emergencyType == null ? null : this.data.emergencyType.Id;
    emergency.Name = this.controls.get("emergencyTypeName")?.value;
    emergency.Protocol = this.getSelectedProtocol();
    return emergency;
  }

  public getSelectedProtocol(): ProtocolDTO {
    return this.availableProtocols.find(protocol => protocol.Id == this.controls.get("protocol")?.value)!;
  }

}
