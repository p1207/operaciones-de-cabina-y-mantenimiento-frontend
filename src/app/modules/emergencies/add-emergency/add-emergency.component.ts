import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmergencyDTO, EmergencyTypeDTO, FlightDTO, ProtocolDTO } from 'src/app/core/model/common.class';
import { EmergencyTypeService } from 'src/app/core/services/emergency-type.service';
import { FlightService } from 'src/app/core/services/flight.service';

@Component({
  selector: 'app-add-emergency',
  templateUrl: './add-emergency.component.html',
  styleUrls: ['./add-emergency.component.css']
})
export class AddEmergencyComponent implements OnInit {
  public controls!: FormGroup;
  public title!: string;
  public availableTypes!: EmergencyTypeDTO[];
  public availableFlights!: FlightDTO[];
  public loadingFlights!: boolean

  constructor(
    private flightService: FlightService,
    private dialogRef: MatDialogRef<AddEmergencyComponent>,
    private emergencyTypeService: EmergencyTypeService,
    @Inject(MAT_DIALOG_DATA) public data: { emergency: EmergencyDTO, title: string },
  ) { }

  ngOnInit(): void {
    this._setFlights();
    this._setTypes();
    this._createControls();
    this._setTitle();
  }

  private _setFlights(): void {
    this.loadingFlights = true;
    this.flightService.getFlights().subscribe(data =>{
      this.availableFlights = data;
      this.loadingFlights = false;
    });
  }

  private _setTypes(): void {
    this.emergencyTypeService.getEmergenciesType().subscribe(data =>{
      this.availableTypes = data;
    });
  }

  private _setTitle() {
    this.title = this.data.title;
  }

  private _createControls() {
    let controls = {
      emergencyName: new FormControl(this.data?.emergency?.Name, Validators.required),
      flight: new FormControl(this.data?.emergency?.FlightId, Validators.required),
      observation: new FormControl(this.data?.emergency?.Observation, Validators.required),
      priority: new FormControl(this.data?.emergency?.Priority, Validators.required),
      emergencyType: new FormControl(this.data?.emergency?.EmergencyType, Validators.required),
      crewMember: new FormControl(this.data?.emergency?.CrewMember, Validators.required),
    }

    this.controls = new FormGroup(controls);
  }

  public invalidFoms(): boolean {
    return this.controls.invalid;
  }

  public disableSaveButton(): boolean | undefined {
    return this.invalidFoms();
  }

  public getTooltipMessage(): string {
    const invalidControlsMessage = "Debe rellenar los campos obligatorios";
    if (this.invalidFoms())
      return invalidControlsMessage;
    return "";
  }

  public getMargin(taskId: number): { [klass: string]: any; } | null {
    let stringIdlenght: number = taskId.toString().length;
    let marginValue: number = 60*stringIdlenght;
    return {'margin-left': marginValue +'px'}
  }

  public closeAndSave(): void {
    this.dialogRef.close(this._getEmergencyDTO());
  }

  private _getEmergencyDTO(): EmergencyDTO {
    let emergency: EmergencyDTO = new EmergencyDTO;
    emergency.Name = this.controls.get("emergencyName")?.value;
    emergency.FlightId=this.controls.get("flight")?.value.FlightId;
    emergency.Observation=this.controls.get("observation")?.value;
    emergency.EmergencyType= this.getSelectedType();
    emergency.Priority=this.controls.get("priority")?.value;
    emergency.CrewMember=this.controls.get("crewMember")?.value;
    return emergency;
  }

  cancel(): void {
    this.dialogRef.close();
  }

  public getSelectedType(): EmergencyTypeDTO {
    return this.availableTypes.find(t => t.Id == this.controls.get("emergencyType")?.value)!;
  }


}
