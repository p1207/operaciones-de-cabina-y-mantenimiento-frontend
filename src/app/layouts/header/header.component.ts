import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmLogoutDialogComponent } from 'src/app/core/dialogs/confirm-logout-dialog/confirm-logout-dialog.component';
import { NotificationService } from 'src/app/core/services/notification.service';
import { UserInfoComponent } from 'src/app/modules/user-info/user-info.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public notificationsQuantity!: number;
  public userId: string = localStorage.getItem("userId")!;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private notificationsService: NotificationService) {
   }

  ngOnInit(): void {
    this._setNotificationsQuantity();

  }

  private _setNotificationsQuantity() {
    this.notificationsService.getNotifications(this.userId).subscribe(data =>{
      this.notificationsQuantity = data?.length
    });
  }

  public redirectHome(){
    this.router.navigate(['/home']);
  }

  public openConfirmLogOutDialog(): void {
    const dialogRef = this.dialog.open(ConfirmLogoutDialogComponent, {
      width: '280px',
      height: '150px'
    });

    dialogRef.afterClosed().subscribe(res => {
      if(res) this.router.navigate(['/login']);
    });
  }

  public openInfoDialog(): void {
    this.dialog.open(UserInfoComponent, {
      width: '700px',
      height: '480px',
    });
  }

}
