import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { ScreenNode } from './model/side-nav-bar.class';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.css']
})
export class SideNavBarComponent implements OnInit {

  public profileText: any = {
    TECHNICIAN : "TÉCNICO",
    SUPERVISOR : "SUPERVISOR",
    AUXILIARY : "AUXILIAR",
    COPILOT : "COPILOTO",
    PILOT : "PILOTO"
  };

  public userProfile: string = localStorage.getItem("profile")!;
  public name: string = localStorage.getItem("name")!;
  public show!: boolean;
  public menu!: ScreenNode[];


  public treeControl = new NestedTreeControl<ScreenNode>(node => node.children);
  public dataSource = new MatTreeNestedDataSource<ScreenNode>();

  constructor( private http: HttpClient ) {
    this.getJSON().subscribe(restrictions => {
      this.menu = restrictions[this.userProfile];
    });
  }

   public getJSON(): Observable<any> {
    return this.http.get("./assets/screenRestrictions.json");
  }

  public ngOnInit(): void { }


  public hasChild = (_: number, node: ScreenNode) => !!node.children && node.children.length > 0;

}
