export interface ScreenNode {
  name: string;
  icon?: string;
  url?: string;
  children?: ScreenNode[];
}
