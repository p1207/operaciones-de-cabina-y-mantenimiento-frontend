import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'datePipe' })
export class AppDatePipe implements PipeTransform {
  constructor() { }

  transform(date: Date | string, displayTime: boolean = false): string {
    date = this.checkUTFFormat(date);
    const format: string = displayTime ? 'dd/MM/YYYY, HH:mm' : 'dd/MM/YYYY';
    date = new Date(date);
    date.setDate(date.getDate());
    return date.toLocaleDateString();
  }

  checkUTFFormat(date: Date | string): Date | string {
    if (/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(date.toString())) {
      return new Date(date + 'T00:00-0800');
    }
    return date;
  }
}
