import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorDialogComponent } from '../dialogs/error-dialog/error-dialog.component';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private router: Router,public dialog: MatDialog) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(error => {
        console.log('Error: ', error);
        this.dialog.open(ErrorDialogComponent, { data: { Error: error } });
        return throwError(error);
      }));
  }
}
