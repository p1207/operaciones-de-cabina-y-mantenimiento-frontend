export enum UserProfiles {
  TECHNICIAN,
  SUPERVISOR
}

export enum MaintenancePriority {
  HIGH,
  MEDIUM,
  LOW
}

export enum MaintenanceRequestStatus {
  REGISTERED,
  INPROGRESS,
  PENDING,
  APPROVED
}

export enum AircrafType {
  CREWED,
  NOTCREWED
}

export enum MaintenanceType {
  CORRECTIVE,
  PREVENTIVE
}

export enum MenuType {
  VEGAN,
  VEGETARIAN,
  CELIAC,
  STANDARD
}

export enum SanitaryType {
  COMFORT,
  CLEANING,
  DISPOSABLE
}

export enum SaleType {
  DRINK,
  PERFUMERY,
  ELECTRONICS,
  JEWELRY,
  GROCERIES
}

export enum ControlType {
  ROUTINE,
  EVENTUAL
}

export enum FlightType {
  REGULAR,
  CHARTER,
  CORPORATE,
  LOW_COST,
  NATIONALS,
  NOT_TRIPULATED,
  INTERNATIONAL,
  INTERCONTINENTAL,
  TRANSOCEANIC,
}

export enum FlightStatus {
  IN_FLIGHT,
  ON_LAND,
  CANCELLED,
}

export enum ControlByFlightStatus {
  PENDING,
  CHECKED
}
