import { AircrafType, ControlByFlightStatus, ControlType, FlightStatus, FlightType, MaintenancePriority, MaintenanceRequestStatus, MaintenanceType, MenuType, SaleType, SanitaryType } from "./enum.class";

export class AircraftDTO {
  public Id!: string;
  public Name!: string;
  public Domain!: number;
  public Model!: ModelDto
  public Type!: string;
  public TypeEnum!: AircrafType;
  public Maintenances!: MaintenanceDTO[];
}

export class ModelDto {
  public Description!: string;
  public Id!: number;
}

export class MaintenanceDTO {
  public Id!: number | null;
  public Name!: string;
  public Active!: boolean;
  public Description!: string;
  public Type!: string;
  public TypeEnum!: MaintenanceType;
  public Supplies!: MaintenanceSupplyDTO[]
  public Tasks!: MaintenanceTaskDTO[];
  public Abilities!: AbilityDTO[];
}

export class MaintenanceTaskDTO {
  public Id!: number | null;
  public Description!: string;
}

export class AbilityDTO {
  public Id!: number;
  public Name!: string;
}

export class MaintenanceSupplyDTO {
  public Id!: number | null;
  public Name!: string
  public Price!: number;
}

export class MaintenanceRequestDTO {
  public Id!: number | null;
  public Description!: string;
  public Date!: Date;
  public Priority!: MaintenancePriority;
  public Module!: number;
  public Status!: MaintenanceRequestStatus;
  public StatusId!: MaintenanceRequestStatus;
  public Observation!: string;
  public Aircraft!: string;
  public Technician!: TechnicianDTO;
  public Tasks!: MaintenanceTaskDTO[];
  public Maintenance!: MaintenanceDTO;

  public AircraftId!: number;
  public TechnicianId!: number;
  public MaintenanceId!: number;
}

export class TechnicianDTO {
  public Id!: number
  public Name!: string;
  public Module!: number;
  public Abilities!: AbilityDTO[];
}

export class SupplyDTO {
  public Id!: number | null;
  public Name!: string;
  public Description!: string;
}

export class SupplyByFlightDTO {
  public Id!: number | null;
  public SupplyId!: number | null;
  public FlightId!: number | null;
  public InitialQuantity!: number;
  public FinalQuantity!: number;
  public Supply!: SupplyDTO;
}

export class CateringDTO extends SupplyDTO {
  public MenuType!: MenuType;
}

export class SaleDTO extends SupplyDTO {
  public DollarSalePrice!: number;
  public PesoSalePrice!: number;
  public SaleType!: SaleType
}

export class SanitaryDTO extends SupplyDTO {
  public SanitaryType!: SanitaryType;
}

export class ControlDTO {
  public Id!: number | null;
  public Name!: string;
  public Type!: string;
  public Description!: string;
  public Role!: RoleDTO;
  public Tasks!: ControlTaskDTO[];
  public Supplies!: ControlSupplyDTO[];
  public FligthId!: number | null;
}

export class RoleDTO {
  public Name!: string;
  public Id!: string;
}

export class ControlTaskDTO {
  public Id!: number | null;
  public Description!: string;
  public Role!: RoleDTO | null;
  public RoleId!: number | null;
}

export class ControlSupplyDTO {
  public Id!: number | null;
  public Name!: string
  public Price!: number;
}

export class FlightDTO {
  public FlightId!: number;
  public Domain!: string;
  public Type!: FlightType;
  public Status!: string;
  public Crew!: CrewMemberDTO[];
}

export class CrewMemberDTO {
  public Id!: number;
  public Name!: string;
  public ModuleValue!: number;
  public Role!: RoleDTO;
}

export class ControlByFlightDTO {
  public Id!: number | null;
  public ControlId!: number;
  public FlightId!: string;
  public CreationDate!: Date;
  public Observation!: string;
  public Status!: string;
  public Control!: ControlDTO;
}

export class SuppliesQuantitiesDTO {
  public CeliacMenu!: number;
  public StandardMenu!: number;
  public VeganMenu!: number;
  public VegetarianMenu!: number;
}

export class PassengerDTO {
  public Diet!: string;
  public Lastname!: string;
  public Seat!: string;
  public Name!: string;
  public Conditio!: string;
  public Class!: string;
}

export class EmergencyDTO {
  public Id!: number;
  public Name!: string;
  public Priority!: string;
  public Observation!: string;
  public EmergencyType!: EmergencyTypeDTO;
  public FlightId!: string;
  public CrewMember!: string;
}

export class ProtocolDTO {
  public Id!: number;
  public Name!: string;
  public Tasks!: ProtocolTaskDTO[];
}

export class ProtocolTaskDTO {
  public Name!: string;
  public Id!: number;
}

export class NotificationDTO {
  public Id!: number;
  public Date!: Date;
  public Priority!: string;
  public Subject!: string;
  public Message!: string;
  public User!: number;
}

export class EmergencyTypeDTO {
  public Id!: number | null;
  public Name!: string;
  public Protocol!: ProtocolDTO;
}

export class AuditoryDTO {
  public Id!: number;
  public Ip!: string;
  public User!: string;
  public Name!: string;
  public Date!: Date;

}

export class MaintenanceByAircraftDTO {
  public Id!: number | null;
  public Aircraft!: string;
  public Maintenance!: MaintenanceDTO;
}

export class UserDTO {
  public Id!: number;
  public Name!: string;
  public Username!: string;
  public Surname!: string;
  public Password!: string
  public Email!: string;
  public Role!: RoleDTO;
  public Abilities!: AbilityDTO[];
  public FlightId?: string;
  public CrewId?: number;
}

export class FlightByCrewMember {
  public FlightId!: string;
	public Aircraft!: string;
  public IdCrew!: string;
	public AircraftId!: string;
  public StatusReason!: string;
  public Status!: number;
}


	/*
	public TheoricalOrigin!: string;


	public RealOrigin!: string;


	public TheoricalDestiny!: string;

	public RoyalDestiny!: string;

	public CompanyName!: string;

	public Status!: string;

	public TheoricalRoute!: string;


	public RoyalRoute!: string;


	public FlightRule!: string;


	public FlightType!: string;

	public TakeOffDay!: string;


	public TakeOffDate!: string;

	public RoyalTakeOffDate!: string;


	public EstimadedLandingDate!: string;

	public ActualLandingDate!: string;

	public EstimatedFuelLiters!: string;

	public ActualFuelLiters!: string;

	public EstimatedLubricant!: string;

	public ActualLubricant!: string;

	public EstimatedMileage!: string;

	public ActualMileage!: string;

	public EstimatedMileage!: string;

	public ActualMileage!: string;

	public IsCheckIn!: string;

	public EstimatedMileage!: string;

	public TotalPeopleOnBoard!: string;

	public OriginalLoadWaight!: string;

	public DestinationLoadWaight!: string;

	public isInformed!: string;


	public PossibleAircraft!: string;

  */
