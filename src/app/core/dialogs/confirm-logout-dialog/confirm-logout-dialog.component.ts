import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-logout-dialog',
  templateUrl: './confirm-logout-dialog.component.html',
  styleUrls: ['./confirm-logout-dialog.component.css']
})
export class ConfirmLogoutDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ConfirmLogoutDialogComponent>,) { }

  ngOnInit(): void {
  }

  public doClose(): void {
    localStorage.clear();
    this.dialogRef.close(true);
  }
}
