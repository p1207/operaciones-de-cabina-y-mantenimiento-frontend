import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ControlSupplyDTO, SupplyDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ControlSupplyService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
  }

  protected getContext(): string {
    return "/Insumos";
  }

  public getSupplies(): Observable<ControlSupplyDTO[]> {
    return this.doGet<ControlSupplyDTO[]>(this.getContext() + '/all') as Observable<ControlSupplyDTO[]>;
  }
}
