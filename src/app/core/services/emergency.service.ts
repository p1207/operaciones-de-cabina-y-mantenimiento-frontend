import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmergencyDTO, ProtocolDTO, ProtocolTaskDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class EmergencyService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
  }

  protected getContext(): string {
    return "/Emergencia";
  }

  public getEmergencies(): Observable<EmergencyDTO[]> {
    return this.doGet<EmergencyDTO[]>(this.getContext() + '/all') as Observable<EmergencyDTO[]>;
  }

  public saveEmergency(req: EmergencyDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + "/save", req) as Observable<any>;
  }
}
