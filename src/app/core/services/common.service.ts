import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AbilityDTO, AircraftDTO, MaintenanceDTO, MaintenanceTaskDTO } from '../model/common.class';
import { AircrafType } from '../model/enum.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService extends BaseService {

  public text = "  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deleniti iste amet ad dolor expedita laborum voluptatum praesentium earum consectetur, omnis, quo explicabo veniam sapiente! Voluptatem nam velit autem laborum."
  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    throw new Error('Method not implemented.');
  }

  //TODO: Borrar mock. Hecho para conseguir n mantenimiento

  public getMaintenances(): MaintenanceDTO[]{
    let mockList: MaintenanceDTO[] = []
    for (let step = 0; step < 5; step++) {
      let mock = new MaintenanceDTO();
      mock.Id = step;
      mock.Active = true;
      mock.Description = this.text;
      mock.Name = "Nombre Mantenimiento " + step;
      mock.TypeEnum = step%2 == 0 ? 0 : 1;
      mock.Tasks = this.getTascks();
      mock.Abilities = [] ;
      mockList.push(mock);
    }
    return mockList;
  }

  public getTascks(): MaintenanceTaskDTO[]{
    let mockList: MaintenanceTaskDTO[] = []
    for (let step = 0; step < 5; step++) {
      let mock = new MaintenanceTaskDTO();
      mock.Id = step;
      mock.Description = "Acción de tarea"
      mockList.push(mock);
    }
    return mockList;
  }

}
