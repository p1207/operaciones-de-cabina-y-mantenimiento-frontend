import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MaintenanceSupplyDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceSupplyService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/InsumoMantenimiento";
  }

  public getSupplies(): Observable<MaintenanceSupplyDTO[]> {
    return this.doGet<MaintenanceSupplyDTO[]>(this.getContext() + '/all') as Observable<MaintenanceSupplyDTO[]>;
  }

}
