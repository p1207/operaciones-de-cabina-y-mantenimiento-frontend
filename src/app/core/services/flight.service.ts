import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FlightDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class FlightService extends BaseService {

  protected getContext(): string {
    return "/Vuelo"
  }

  public getFlights(): Observable<FlightDTO[]> {
    return this.doGet<FlightDTO[]>(this.getContext() + '/all') as Observable<FlightDTO[]>;
  }

  public getFlightStatus(flightCode: string): Observable<FlightDTO> {
    return this.doGet<FlightDTO>(this.getContext() + `/estado/${flightCode}`) as Observable<FlightDTO>;
  }

}
