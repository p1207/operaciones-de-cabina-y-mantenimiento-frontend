import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ControlByFlightDTO, ControlDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ControlService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
  }

  protected getContext(): string {
    return "/Controles";
  }

  private _getControlByFlightContext(): string {
    return "/ControlXVuelo"
  }

  public getControls(): Observable<ControlDTO[]> {
    return this.doGet<ControlDTO[]>(this.getContext() + '/all') as Observable<ControlDTO[]>;
  }

  public deleteControl(id: number | null): Observable<void> {
    return this.doDelete<void>(this.getContext() + "/delete", `/${id}`) as Observable<void>;
  }

  public saveControl(req: ControlDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + "/save", req) as Observable<any>;
  }

  public getControlsByFlight(flightId: string): Observable<any> {
    return this.doGet<any>(this._getControlByFlightContext() + `/allByVuelo/${flightId}`) as Observable<any>;
  }

  public deleteControlByFlight(id: number | null): Observable<void> {
    return this.doDelete<void>(this._getControlByFlightContext() + "/delete", `/${id}`)
  }

  public saveControlByFlight(control: ControlByFlightDTO): Observable<void> {
    return this.doPost<void>(this._getControlByFlightContext() + "/save", control) as Observable<any>;
  }

  public updateControlByFlight(control: ControlByFlightDTO | undefined): Observable<void>{
    return this.doPut<void>(this._getControlByFlightContext() + "/update", control) as Observable<any>;
  }
}
