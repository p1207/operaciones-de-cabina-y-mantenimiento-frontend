import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NotificationDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService extends BaseService {

  public notificationsQuantity!: number;

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  public getNotifications(userId: string): Observable<NotificationDTO[]> {
    return this.doGet<NotificationDTO[]>(this.getContext() + `/user/${userId}`) as Observable<NotificationDTO[]>;
  }

  public deleteNotification(notificationId: string): Observable<void> {
    return super.doDelete<void>(this.getContext() + "/delete", `/${notificationId}`) as Observable<void>;
  }

  protected getContext(): string {
    return "/Notificacion";
  }


}
