import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AircraftDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AircraftService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/Aeronaves"
  }

  public getAircrafts(): Observable<AircraftDTO[]> {
    return this.doGet<AircraftDTO[]>(this.getContext() + '/all') as Observable<AircraftDTO[]>;
  }


}
