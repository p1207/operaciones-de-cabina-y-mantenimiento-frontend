import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FlightByCrewMember, UserDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService {

  protected getContext(): string {
    return "/Usuarios";
  }

  public tryLogin(req: UserDTO): Observable<UserDTO> {
    return this.doPost<UserDTO>(this.getContext() + "/autenticacion", req) as Observable<UserDTO>;
  }

  public getFlight(id: any): Observable<FlightByCrewMember[]> {
    return this.doPost<FlightByCrewMember[]>(this.getContext() + `/getVueloByTripulante`,id) as Observable<FlightByCrewMember[]>;
  }

}
