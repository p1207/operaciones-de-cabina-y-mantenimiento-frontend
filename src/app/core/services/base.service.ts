import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export abstract class BaseService {

  protected baseURI = environment.webAPIUrl;

  constructor(protected httpClient: HttpClient) {
    this.baseURI = environment.webAPIUrl;
  }

  protected getEndpointApi(): string {
    return this.baseURI + this.getContext();
  }

  protected doPost<T>(apiMethod: string, data: any): Observable<Object> {
    var headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
    return this.httpClient.post(this.baseURI + apiMethod , data , { headers: headers });
  }

  protected doGet<T>(apiMethod: string): Observable<T> {
    return this.httpClient.get<T>(this.baseURI + apiMethod);
  }

  protected doDelete<T>(apiMethod: string, id: string): Observable<T> {
    return this.httpClient.delete<T>(this.baseURI + apiMethod + id);
  }

  protected doPut<T>(apiMethod: string, data: any): Observable<T> {
    var headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
    return this.httpClient.put<T>(this.baseURI + apiMethod , data , { headers: headers });
  }

  protected abstract getContext(): string;
}
