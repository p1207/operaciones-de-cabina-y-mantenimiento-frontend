import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmergencyTypeDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class EmergencyTypeService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
  }

  protected getContext(): string {
    return "/TipoEmergencia";
  }

  public getEmergenciesType(): Observable<EmergencyTypeDTO[]> {
    return this.doGet<EmergencyTypeDTO[]>(this.getContext() + '/all') as Observable<EmergencyTypeDTO[]>;
  }

  public saveEmergencyType(req: EmergencyTypeDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + "/save", req) as Observable<any>;
  }

  public deleteEmergencyType(id: number | null): Observable<void> {
    return this.doDelete<void>(this.getContext() + "/delete", `/${id}`) as Observable<void>;
  }

}
