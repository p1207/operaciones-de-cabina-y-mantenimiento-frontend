import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AbilityDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AbilityService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/Habilidades";
  }

  public getAbilities(): Observable<AbilityDTO[]> {
    return this.doGet<AbilityDTO[]>(this.getContext() + '/all') as Observable<AbilityDTO[]>;
  }


}
