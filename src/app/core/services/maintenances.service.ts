import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MaintenanceByAircraftDTO, MaintenanceDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class MaintenancesService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/Mantenimientos";
  }

  public getMaintenances(): Observable<MaintenanceDTO[]> {
    return this.doGet<MaintenanceDTO[]>(this.getContext() + '/all') as Observable<MaintenanceDTO[]>;
  }

  public deleteMaintenance(id: number | null): Observable<void> {
    return this.doDelete<void>(this.getContext() + "/delete", `/${id}`) as Observable<void>;
  }

  public saveMaintenance(req: MaintenanceDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + "/save", req) as Observable<any>;
  }

  public saveMaintenanceByAircraft(req: MaintenanceByAircraftDTO) : Observable<void> {
    return this.doPost<void>(this.getContext() + "/saveAeronaveXMantenimiento", req) as Observable<any>;
  }

  public getMaintenancesByAircraft(id: string): Observable<MaintenanceByAircraftDTO[]> {
    return this.doGet<MaintenanceByAircraftDTO[]>(this.getContext() + `/allMantenimientosByAeronave/${id}`) as Observable<MaintenanceByAircraftDTO[]>;
  }


}
