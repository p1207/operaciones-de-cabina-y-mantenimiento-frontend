import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TechnicianDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class TechnicianService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/Tecnicos"
  }

  public getTechnicians(): Observable<TechnicianDTO[]> {
    return this.doGet<TechnicianDTO[]>(this.getContext() + '/all') as Observable<TechnicianDTO[]>;
  }
}
