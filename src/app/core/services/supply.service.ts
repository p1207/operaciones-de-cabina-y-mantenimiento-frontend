import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CateringDTO, PassengerDTO, SaleDTO, SanitaryDTO, SuppliesQuantitiesDTO, SupplyByFlightDTO, SupplyDTO } from '../model/common.class';
import { MenuType, SaleType, SanitaryType } from '../model/enum.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class SupplyService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/Insumo"
  }

  public getCateringSupplies(): Observable<CateringDTO[]> {
    return this.doGet<CateringDTO[]>(this.getContext() + "/Catering/all") as Observable<CateringDTO[]>;
  }

  public getSaleSupplies(): Observable<SaleDTO[]> {
    return this.doGet<SaleDTO[]>(this.getContext() + "/Venta/all") as Observable<SaleDTO[]>;
  }

  public getSanitarySupplies(): Observable<SanitaryDTO[]> {
    return this.doGet<SanitaryDTO[]>(this.getContext() + "/Sanitario/all") as Observable<SanitaryDTO[]>;
  }

  public getSuppliesByFlight(flightId: string): Observable<SupplyByFlightDTO[]> {
    return this.doGet<SupplyByFlightDTO[]>(this.getContext() + `/Vuelo/all/${flightId}`) as Observable<SupplyByFlightDTO[]>;
  }

  public getSuppliesRecommendedQuantities(flightId: string): Observable<SuppliesQuantitiesDTO> {
    return this.doGet<SuppliesQuantitiesDTO>(this.getContext() + `/Catering/menus/${flightId}`) as Observable<SuppliesQuantitiesDTO>;
  }

  public getPassengers(flightId: string): Observable<PassengerDTO[]> {
    return this.doGet<PassengerDTO[]>(this.getContext() + `/Catering/pasajeros/${flightId}`) as Observable<PassengerDTO[]>;
  }

  public saveSupply(supply: SupplyDTO, url:string): Observable<void> {
    return this.doPost<void>(this.getContext() + url, supply) as Observable<any>;
  }

  public updateSupplyQuantity(supply: SupplyByFlightDTO): Observable<void> {
    return this.doPut<void>(this.getContext() + "/Vuelo/updateCantidadFinal", supply) as Observable<void>
  }

  public saveSupplyByFlight(supply: SupplyByFlightDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + "/Vuelo/nuevoItemVuelo", supply) as Observable<any>;
  }

  public deleteSupplyByFlight(supplyId: number | null): Observable<void> {
    return this.doDelete<void>(this.getContext() + "/Vuelo/delete", `/${supplyId}`) as Observable<any>;
  }


}
