import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuditoryDTO, ControlByFlightDTO, ControlDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AuditService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
  }

  protected getContext(): string {
    return "/Auditoria";
  }

  public getAll(): Observable<AuditoryDTO[]> {
    return this.doGet<AuditoryDTO[]>(this.getContext() + '/all') as Observable<AuditoryDTO[]>;
  }

}
