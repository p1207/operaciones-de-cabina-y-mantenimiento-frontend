import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MaintenanceRequestDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceRequestService extends BaseService {

  constructor(protected override httpClient: HttpClient){
    super(httpClient);
    this.baseURI = environment.webAPIUrl;
  }

  protected getContext(): string {
    return "/Solicitudes";
  }

  public getMaintenanceRequests(): Observable<MaintenanceRequestDTO[]> {
    return this.doGet<MaintenanceRequestDTO[]>(this.getContext() + '/all') as Observable<MaintenanceRequestDTO[]>;
  }

  public saveMaintenanceRequest(request: MaintenanceRequestDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + '/save', request) as Observable<any>;
  }

  public getTechnicMaintenanceRequests(id: number): Observable<MaintenanceRequestDTO[]> {
    return this.doGet<MaintenanceRequestDTO[]>(this.getContext() + `/RequestByIdTecnico/${id}`) as Observable<MaintenanceRequestDTO[]>;
  }




}
