import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RoleDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends BaseService {

  protected getContext(): string {
    return "/Controles";
  }

  public getRoles(): Observable<RoleDTO[]> {
    return this.doGet<RoleDTO[]>(this.getContext() + '/allRoles') as Observable<RoleDTO[]>;
  }

}
