import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProtocolDTO } from '../model/common.class';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ProtocolService extends BaseService {

  protected getContext(): string {
    return '/Protocolo';
  }

  public getProtocols(): Observable<ProtocolDTO[]> {
    return this.doGet<ProtocolDTO[]>(this.getContext() + '/all') as Observable<ProtocolDTO[]>;
  }

  public saveProtocol(req: ProtocolDTO): Observable<void> {
    return this.doPost<void>(this.getContext() + "/save", req) as Observable<any>;
  }


}
